// 2DMissionEditorView.h : interface of the CPerspectiveView class
//
#include "OpenGLWnd.h"

#pragma once


class CPerspectiveView : public COpenGLWnd
{
protected: // create from serialization only
	CPerspectiveView();
	DECLARE_DYNCREATE(CPerspectiveView)



	int	m_lastMouseX,
		m_lastMouseY;
	CPoint m_ptLeftDownPos;

	double m_Modelview[ 16 ];
	double m_Projection[ 16 ];
	int m_Viewport[ 4 ];


public:
	CMFC_Joe3DExporterDoc* GetDocument() const;

// Operations
public:

// Overrides
public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void RenderScene( );
	virtual afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	virtual bool InitOpenGL( );
// Implementation
public:
	virtual ~CPerspectiveView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
public:
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
protected:
	virtual void OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/);
public:
	afx_msg void OnMButtonDown(UINT nFlags, CPoint point);
public:
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
};

#ifndef _DEBUG  // debug version in 2DMissionEditorView.cpp
inline CMFC_Joe3DExporterDoc* CPerspectiveView::GetDocument() const
   { return reinterpret_cast<CMFC_Joe3DExporterDoc*>(m_pDocument); }
#endif

