#pragma once
#include "afxcmn.h"
#include "resource.h"

// CProgressDlg dialog

class CProgressDlg : public CDialog
{
	DECLARE_DYNAMIC(CProgressDlg)

  protected:
	int min;
	int max;
	int step;
	CProgressCtrl m_ProgressBar;
	CString m_StatusText;

  public:
	CProgressDlg( int nMin, int nMax, int nStep, CWnd* pParent = NULL );   // standard constructor
	virtual ~CProgressDlg( );

	void stepIt( );
	void setProgress( int position );
	bool isDone( );

// Dialog Data
	enum { IDD = IDD_PROGRESS_DIALOG };

  protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

public:
	virtual BOOL OnInitDialog();
};
