// MFC_Joe3DExporterView.h : interface of the CMFC_Joe3DExporterView class
//


#pragma once


class CMFC_Joe3DExporterView : public CView
{
protected: // create from serialization only
	CMFC_Joe3DExporterView();
	DECLARE_DYNCREATE(CMFC_Joe3DExporterView)

// Attributes
public:
	CMFC_Joe3DExporterDoc* GetDocument() const;

// Operations
public:

// Overrides
public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// Implementation
public:
	virtual ~CMFC_Joe3DExporterView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in MFC_Joe3DExporterView.cpp
inline CMFC_Joe3DExporterDoc* CMFC_Joe3DExporterView::GetDocument() const
   { return reinterpret_cast<CMFC_Joe3DExporterDoc*>(m_pDocument); }
#endif

