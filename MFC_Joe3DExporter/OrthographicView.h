#pragma once
#include "OpenGLWnd.h"

// COrthographicView view

class COrthographicView : public COpenGLWnd
{
	DECLARE_DYNCREATE(COrthographicView)

protected:
	COrthographicView();           // protected constructor used by dynamic creation
	virtual ~COrthographicView();
	virtual void SetFrustum( );
	void RenderScene( );
	virtual bool InitOpenGL( );

	CPoint m_ptLeftDownPos;


public:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	virtual void OnInitialUpdate();
};


