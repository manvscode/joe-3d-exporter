// OpenGLWnd.cpp : implementation file
//

#include "stdafx.h"
#include "OpenGLWnd.h"
#include "Vertex.h"

#include "GLRCSharer.h"

#include <afx.h>
// COpenGLWnd

IMPLEMENT_DYNCREATE(COpenGLWnd, CView)

COpenGLWnd::COpenGLWnd()
	: m_zoom(0.05f)
{
	m_RenderScene = mainRender; 
	m_dNearPlane   = 0.0001; 
	m_dFarPlane    = 100.0;
	m_bRenderWireframe = false;	
}

COpenGLWnd::~COpenGLWnd()
{
}

BEGIN_MESSAGE_MAP(COpenGLWnd, CView)
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_CREATE()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()


// COpenGLWnd drawing
bool COpenGLWnd::SetDCPixelFormat(HDC hDC, DWORD dwFlags)
{

	// pfd Tells Windows How We Want Things To Be
	PIXELFORMATDESCRIPTOR pixelDesc =
	{
		sizeof(PIXELFORMATDESCRIPTOR),// Pixel Format Descriptor Size
		1, // Version Number
		PFD_DRAW_TO_WINDOW | // Format Must Support Window
		PFD_SUPPORT_OPENGL | // Format Must Support OpenGL
		PFD_DOUBLEBUFFER, // Must Support Double Buffering
		PFD_TYPE_RGBA, // Request An RGBA Format
		32, // Select Our Color Depth
		0, 0, 0, 0, 0, 0, // Color Bits Ignored
		8, // Alpha Buffer
		0, // Shift Bit Ignored
		0, // No Accumulation Buffer
		0, 0, 0, 0, // Accumulation Bits Ignored
		16, // 16Bit Z-Buffer (Depth Buffer)
		0, // No Stencil Buffer
		0, // No Auxiliary Buffer
		PFD_MAIN_PLANE, // Main Drawing Layer
		0, // Reserved
		0, 0, 0 // Layer Masks Ignored
	};
	
	int nPixelIndex = ::ChoosePixelFormat(hDC, &pixelDesc);
	if (nPixelIndex == 0) // Choose default
	{
		nPixelIndex = 1;
		if (::DescribePixelFormat(hDC, nPixelIndex, 
			sizeof(PIXELFORMATDESCRIPTOR), &pixelDesc) == 0)
			return false;
	}

	if (!::SetPixelFormat(hDC, nPixelIndex, &pixelDesc))
		return false;

	return true;
}

bool COpenGLWnd::InitOpenGL()
{
	//Get a DC for the Client Area
	m_hDC = new CClientDC(this);
	
	//Failure to Get DC
	if( m_hDC == NULL )
		return false;


	if( !SetDCPixelFormat(m_hDC->GetSafeHdc(), PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER | PFD_DRAW_TO_BITMAP) )
		return false;


	//Create Rendering Context
	m_hRC = ::wglCreateContext(m_hDC->GetSafeHdc() );

	//Failure to Create Rendering Context
	if( m_hRC == 0 )
		return false;

	//Make the RC Current
	if( ::wglMakeCurrent( m_hDC->GetSafeHdc(), m_hRC ) == FALSE )
		return false;

	int err = 0;
	if( (err = glGetError( )) != GL_NO_ERROR )
	{
		CString error;
		error.Format( _T("There was an OpenGL error. Error code %d. Will attempt to continue anyway..."), err );
		AfxMessageBox( error );
	}


	if( (err = glGetError( )) != GL_NO_ERROR )
	{
		CString error;
		error.Format( _T("There was an OpenGL error. Error code %d. Will attempt to continue anyway..."), err );
		AfxMessageBox( error );
	}

	GLenum glewError = glewInit( );
	if( GLEW_OK != glewError )
	{
		MessageBox( _T("There was a problem probing the graphics capabilities of your video card."), _T("Error"), MB_OK | MB_ICONERROR );
		return false;
	}

	CGLRCSharer *pRCSharer = CGLRCSharer::getInstance();
	pRCSharer->addRC( m_hRC );

	SetGLConfig( );


	return true;
}

void COpenGLWnd::SetGLConfig( )
{
	int err = 0;
	if( (err = glGetError( )) != GL_NO_ERROR )
	{
		CString error;
		error.Format( _T("There was an OpenGL error. Error code %d. Will attempt to continue anyway..."), err );
		AfxMessageBox( error );
	}

	glEnable( GL_LINE_STIPPLE );

	glClearColor( 0.2f, 0.2f, 0.2f, 1.0f );
	glClearDepth( 1.0f );
	//glEnable( GL_NORMALIZE );
	glEnable( GL_DEPTH_TEST );
	glDepthFunc( GL_LEQUAL );
	glEnable( GL_LIGHTING );
	glEnable( GL_LIGHT0 );
	glLightfv( GL_LIGHT0, GL_POSITION, light_position );
	glLightfv( GL_LIGHT0, GL_SPOT_DIRECTION, light_vector );


	glPointSize( 3.0f );
	glEnable( GL_TEXTURE_2D );
	glEnableClientState( GL_VERTEX_ARRAY );
	glEnableClientState( GL_NORMAL_ARRAY );
	glEnableClientState( GL_TEXTURE_COORD_ARRAY );

	glEnable( GL_COLOR_MATERIAL );
	glFrontFace( GL_CCW );
	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA,  GL_ONE_MINUS_SRC_ALPHA );





	glPixelStorei( GL_PACK_ALIGNMENT, 4 );
	glPixelStorei( GL_UNPACK_ALIGNMENT, 4 );
	glShadeModel( GL_SMOOTH ); //GL_FLAT	
	glLineWidth( 1.0f );
	//glPointSize( 1.0f );
	glEnable( GL_POINT_SMOOTH );
	glEnable( GL_LINE_SMOOTH );

	glHint( GL_POINT_SMOOTH_HINT, GL_FASTEST /*GL_NICEST*/);  // if slow change to GL_FASTEST
	glHint( GL_LINE_SMOOTH_HINT, GL_FASTEST/*GL_NICEST*/ );  
	glHint( GL_PERSPECTIVE_CORRECTION_HINT, /*GL_FASTEST*/GL_NICEST );

	if( (err = glGetError( )) != GL_NO_ERROR )
	{
		CString error;
		error.Format( _T("There was an OpenGL error. Error code %d. Will attempt to continue anyway..."), err );
		AfxMessageBox( error );
	}
}

void COpenGLWnd::SetGLPickingConfig( )
{
	int err = 0;
	if( (err = glGetError( )) != GL_NO_ERROR )
	{
		CString error;
		error.Format( _T("There was an OpenGL error. Error code %d. Will attempt to continue anyway..."), err );
		AfxMessageBox( error );
	}
	//glClearColor( 1.0f, 1.0f, 1.0f, 1.0f );
	glEnable( GL_LINE_STIPPLE );

	glClearColor( 0.0f, 0.0f, 0.0f, 1.0f );
	glClearDepth(1.0f);
	glDisable(GL_NORMALIZE);
	glEnable( GL_DEPTH_TEST );
	glDepthFunc(GL_LEQUAL);
	glInitNames( );
	glDisable(GL_LIGHTING);
	glDisable(GL_LIGHT0);

	glFrontFace( GL_CW );
	glDisable(GL_BLEND);
	glBlendFunc (GL_SRC_ALPHA,  GL_ONE_MINUS_SRC_ALPHA);

	glDisable( GL_TEXTURE_2D );

	glPixelStorei( GL_PACK_ALIGNMENT, 4 );
	glPixelStorei( GL_UNPACK_ALIGNMENT, 4 );

	glShadeModel( GL_FLAT ); 	
	glLineWidth( 1.0f );
	glDisable( GL_POINT_SMOOTH );
	glDisable( GL_LINE_SMOOTH );

	glEnableClientState( GL_VERTEX_ARRAY );
	glEnableClientState( GL_NORMAL_ARRAY );
	glEnableClientState( GL_COLOR_ARRAY );
	glEnableClientState( GL_TEXTURE_COORD_ARRAY );

	if( (err = glGetError( )) != GL_NO_ERROR )
	{
		CString error;
		error.Format( _T("There was an OpenGL error. Error code %d. Will attempt to continue anyway..."), err );
		AfxMessageBox( error );
	}
}

void COpenGLWnd::DrawLine(VERTEX *v1, VERTEX *v2)
{
	glPushMatrix();
	glBegin(GL_LINE_STRIP);
		glVertex3f(v1->v[0], v1->v[1], v1->v[2]);
		glVertex3f(v2->v[0], v2->v[1], v2->v[2]);
	glEnd();
	glPopMatrix();

}

void COpenGLWnd::DrawGrid(const float grid_size )  
{
	VERTEX v1, v2;
	//Grid Size

	// Do the col lines
	glPushMatrix( );
	glPushAttrib( GL_COLOR_BUFFER_BIT | GL_LINE_BIT );

	//Grid Color
	glColor4f(1.0f, 1.0f, 1.0f, 0.4f); //slightly transparent
	glLineWidth( 0.5 );

	for(float col = 0.0f; col <= grid_size ; col++){

		if( col == grid_size / 2.0f )
		{
			glColor4f(0.5f, 0.0f, 0.0f, 0.85f);
			for(float tick = -grid_size / 2.0f; tick <= grid_size / 2.0f ; tick++){
				if( tick == 0.0f )
					continue;
				v1.v[0] = (grid_size / 2.0f) - col - 0.05f;
				v1.v[1] = 0.0f;
				v1.v[2] = tick;

				v2.v[0] = (grid_size / 2.0f) - col + 0.05f;
				v2.v[1] = 0.0f;
				v2.v[2] = tick;
				glLineWidth(1.5f);
				DrawLine(&v1, &v2);
			}
		}
		else
			glColor4f(0.6f, 0.6f, 0.6f, 0.08f);

		v1.v[0] = (grid_size / 2.0f) - col;
		v1.v[1] = 0.0f;
		v1.v[2] = (grid_size / 2.0f);

		v2.v[0] = (grid_size / 2.0f) - col;
		v2.v[1] = 0.0f;
		v2.v[2] = -(grid_size / 2.0f);
		glLineWidth(0.5f);
		DrawLine(&v1, &v2);
	}


	// Do the row lines
	for(float row = 0; row <= grid_size ; row++){
		if( row == grid_size / 2.0f )
		{
			glColor4f(0.5f, 0.0f, 0.0f, 0.85f);
			for(float tick =  -grid_size / 2.0f; tick <= grid_size / 2.0f ; tick++){
				if( tick == 0.0f )
					continue;
				v1.v[0] = tick;
				v1.v[1] = 0;
				v1.v[2] = (grid_size / 2.0f) - row + 0.05f;

				v2.v[0] = tick;
				v2.v[1] = 0;
				v2.v[2] = (grid_size / 2.0f) - row - 0.05f;
				glLineWidth(1.5f);
				DrawLine(&v1, &v2);
			}
		}
		else
			glColor4f(0.5f, 0.5f, 0.5f, 0.05f);
		v1.v[0] = (grid_size / 2.0f);
		v1.v[1] = 0.0f;
		v1.v[2] = (grid_size / 2.0f) - row;

		v2.v[0] = -(grid_size / 2.0f);
		v2.v[1] = 0.0f;
		v2.v[2] = (grid_size / 2.0f) - row;
		glLineWidth(0.5f);
		DrawLine(&v1, &v2);
	}
	
	glPopAttrib( );
	glPopMatrix( );

}

void COpenGLWnd::DrawOriginAxes(const float axis_size)
{
	VERTEX origin, vx, vy, vz;
	//const unsigned int axis_size = 30.0;
	const float arrow_size = 0.25;


	origin.v[0] = 0.0f;
	origin.v[1] = 0.0f;
	origin.v[2] = 0.0f;

	vx.v[0] = axis_size;
	vx.v[1] = 0.0f;
	vx.v[2] = 0.0f;

	vy.v[0] = 0.0f;
	vy.v[1] = axis_size;
	vy.v[2] = 0.0f;

	vz.v[0] = 0.0f;
	vz.v[1] = 0.0f;
	vz.v[2] = axis_size;
	
	glLineWidth( 2.5f );
	glPushMatrix();
	glBegin(GL_LINE_STRIP);
		// X
		glColor4f(1.0f, .0f, 0.0f, 1.0f);
		glVertex3f(origin.v[0], origin.v[1], origin.v[2]);
		glVertex3f(vx.v[0], vx.v[1], vx.v[2]);
		// Y
		glColor4f(0.0f, 1.0f, 0.0f, 1.0f);
		glVertex3f(origin.v[0], origin.v[1], origin.v[2]);
		glVertex3f(vy.v[0], vy.v[1], vy.v[2]);
		// Z
		glColor4f(0.0f, 0.0f, 1.0f, 1.0f);
		glVertex3f(origin.v[0], origin.v[1], origin.v[2]);
		glVertex3f(vz.v[0], vz.v[1], vz.v[2]);

	glEnd();
	glPopMatrix();

	//Draw Arrow points  --->
	glPushMatrix();
	glBegin(GL_TRIANGLES);
		glColor4f(0.0f, 1.0f, 0.0f, 1.0f);
		glVertex3f(arrow_size/2.0f, axis_size , 0.0f);
		glVertex3f(0.0f, axis_size + arrow_size, 0.0f);
		glVertex3f(-arrow_size/2.0f, axis_size, 0.0f);

		glColor4f(1.0f, 0.0f, 0.0f, 1.0f);
		glVertex3f(axis_size, -arrow_size/2.0f, 0.0f);
		glVertex3f(axis_size + arrow_size, 0.0f, 0.0f);
		glVertex3f(axis_size, arrow_size/2.0f , 0.0f);

		glColor4f(0.0f, 0.0f, 1.0f, 1.0f);
		glVertex3f(arrow_size/2.0f,  0.0f, axis_size);
		glVertex3f(0.0f, 0.0f, axis_size + arrow_size);
		glVertex3f(-arrow_size/2.0f, 0.0f, axis_size);

	glEnd();
	glPopMatrix();
	glLineWidth( 1.0f );
}	

void COpenGLWnd::OnDraw(CDC* pDC)
{
	CDocument* pDoc = GetDocument();
	// TODO: add draw code here
	//SetContext( );
	COpenGLWnd::RenderScene( );
	SwapGLBuffers( );
	//glFlush( );
}


// COpenGLWnd diagnostics

#ifdef _DEBUG
void COpenGLWnd::AssertValid() const
{
	CView::AssertValid();
}

void COpenGLWnd::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif //_DEBUG


// COpenGLWnd message handlers

void COpenGLWnd::OnSize(UINT nType, int cx, int cy)
{
	CView::OnSize(nType, cx, cy);

	// TODO: Add your message handler code here

	int height, width;

	width = cx;

	if( cy == 0 )
		height = 1;
	else 
		height = cy;
	
	glViewport( 0,0, width, height );
	SetFrustum( );
	glFlush( );
}

void COpenGLWnd::OnDestroy()
{
	CView::OnDestroy();

	// TODO: Add your message handler code here
	wglMakeCurrent( NULL, NULL );
	wglDeleteContext( m_hRC );

	if( m_hDC )
		delete m_hDC;
	m_hDC = NULL;
}

int COpenGLWnd::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CView::OnCreate(lpCreateStruct) == -1)
		return -1;

	if ( !InitOpenGL() )
	{
		MessageBox( _T("Error setting up OpenGL!"), _T("Init Error!"), MB_OK | MB_ICONERROR );
		return -1;
	}
	return 0;
}
void COpenGLWnd::RenderScene( )
{
	if( m_RenderScene != NULL )
	{
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
		//SetGLConfig();
		DrawOriginAxes( );
		DrawGrid( 300 );
		glColor4f( 0.5f, 0.5f, 0.5f, 1.0f );

		glLightfv( GL_LIGHT0, GL_POSITION, light_position );
		glLightfv( GL_LIGHT0, GL_SPOT_DIRECTION, light_vector );

		if( isRenderModeInWireframe( ) )
			glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
		else
			glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );

		m_RenderScene( (CMFC_Joe3DExporterDoc*)(GetDocument()) );

	}
	else
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
}

void COpenGLWnd::SetFrustum( )
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	//gluPerspective(45.0, m_dAspect, m_dNearPlane, m_dFarPlane); 
	glOrtho( -1.0, 1.0, -1.0, 1.0, 0.0, 30.0 );
	glMatrixMode(GL_MODELVIEW);
}

BOOL COpenGLWnd::OnEraseBkgnd(CDC* pDC)
{
	/*
	 *   Avoid erasing the background as it will 
	 *	 cause flickering...
	 */
	return CView::OnEraseBkgnd(pDC);
}

BOOL COpenGLWnd::PreCreateWindow(CREATESTRUCT& cs)
{
	// This is needed!!!!
	/*cs.style |= (WS_CLIPCHILDREN | WS_CLIPSIBLINGS);*/
	cs.lpszClass = ::AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS | CS_OWNDC,
	::LoadCursor(NULL, IDC_ARROW), NULL, NULL);
	cs.style |= WS_CLIPSIBLINGS | WS_CLIPCHILDREN;
	return CView::PreCreateWindow(cs);
}

void COpenGLWnd::OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/)
{
	// TODO: Add your specialized code here and/or call the base class
}

void COpenGLWnd::setWireFrameRenderMode( const bool bWireframe /*= true*/ )
{
	m_bRenderWireframe = bWireframe;
}

bool COpenGLWnd::isRenderModeInWireframe( ) const
{
	return m_bRenderWireframe;
}

void COpenGLWnd::setTranslationVector( const float x, const float y, const float z )
{
	m_xTranslation = x;
	m_yTranslation = y;
	m_zTranslation = z;
}
