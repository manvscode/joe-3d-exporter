// Side.cpp : implementation file
//

#include "stdafx.h"
#include "MFC_Joe3DExporter.h"
#include "Side.h"



// CSide

IMPLEMENT_DYNCREATE(CSide, COrthographicView)

CSide::CSide()
: COrthographicView( )
{

}

CSide::~CSide()
{
}

BEGIN_MESSAGE_MAP(CSide, COrthographicView)
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()


// CSide drawing

void CSide::OnDraw(CDC* pDC)
{
	CMFC_Joe3DExporterDoc* pDoc = reinterpret_cast<CMFC_Joe3DExporterDoc *>( GetDocument( ) );
	SetContext( );
	glLoadIdentity( );

	glRotatef(90.0f, 0.0f, 1.0f, 0.0f );
	glDisable( GL_CULL_FACE );
	COrthographicView::OnDraw( pDC );
	glEnable( GL_CULL_FACE );
}


// CSide diagnostics

#ifdef _DEBUG
void CSide::AssertValid() const
{
	COrthographicView::AssertValid();
}

#ifndef _WIN32_WCE
void CSide::Dump(CDumpContext& dc) const
{
	COrthographicView::Dump(dc);
}
#endif
#endif //_DEBUG


// CSide message handlers

void CSide::OnMouseMove(UINT nFlags, CPoint point)
{
	CRect cr;
	GetClientRect( &cr );

	if( nFlags & MK_CONTROL )
	{
		if( nFlags & MK_RBUTTON ) // zoom 
		{
			CPoint ptZoom = m_ptLeftDownPos - point;
			m_ptLeftDownPos = point;
			m_zoom += ptZoom.y * 0.005f;
			InvalidateRect(cr, false);
		}
		else if( nFlags & MK_MBUTTON ) // pan
		{
			CPoint ptPan = m_ptLeftDownPos - point;
			m_ptLeftDownPos = point;
			m_zTranslation -= ptPan.x * (1.0f / m_zoom) * 0.001f;
			m_yTranslation += ptPan.y * (1.0f / m_zoom) * 0.001f;
			InvalidateRect(cr, false);
		}
	}

	COrthographicView::OnMouseMove(nFlags, point);
}

void CSide::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint)
{
	switch( lHint )
	{
		case UPD_OPENGL:
		{
			CRect cr;
			GetClientRect( &cr );
			InvalidateRect( cr, false );
			break;
		}
		default:
			break;
	}
}
