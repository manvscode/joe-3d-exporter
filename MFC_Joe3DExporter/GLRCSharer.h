#pragma once
#ifndef _GLRCSHARER_H_
#define _GLRCSHARER_H_
#include "stdafx.h"
#include <afx.h>
#include <afxtempl.h>

class CGLRCSharer
{
  protected:
	static CGLRCSharer *m_pInstance;
	CArray<HGLRC> m_RCArray;

	CGLRCSharer( );

  public:
	static CGLRCSharer *getInstance( );
	virtual ~CGLRCSharer( );

	void addRC( HGLRC hGLRC );
	void shareContexts( );
};


#endif