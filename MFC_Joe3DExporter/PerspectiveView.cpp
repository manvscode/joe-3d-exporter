// 2DMissionEditorView.cpp : implementation of the CPerspectiveView class
//

#include "stdafx.h"
#include "MFC_Joe3DExporter.h"
#include <cmath>
#include "MFC_Joe3DExporterDoc.h"
#include "PerspectiveView.h"
#include <list>

using namespace std;
//using namespace Game;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CPerspectiveView

IMPLEMENT_DYNCREATE(CPerspectiveView, COpenGLWnd)

BEGIN_MESSAGE_MAP(CPerspectiveView, COpenGLWnd)
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, &COpenGLWnd::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &COpenGLWnd::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &COpenGLWnd::OnFilePrintPreview)
	ON_WM_SIZE()
	ON_WM_KEYDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_ERASEBKGND()
	ON_WM_LBUTTONDOWN()
	ON_WM_MBUTTONDOWN()
	ON_WM_RBUTTONDOWN()
END_MESSAGE_MAP()

// CPerspectiveView construction/destruction

CPerspectiveView::CPerspectiveView()
 : COpenGLWnd( )
{
	// TODO: add construction code here
	m_zoom = 0.25f;
	m_xTranslation = 0.0;
	m_yTranslation = -10.0;
	m_zTranslation = -5.0;
	m_xRotation = 60.0;
	m_yRotation = 0.0;
	m_zRotation = 0.0;
	m_dAspect = 1.0;
	m_dNearPlane = 5.0;
	m_dFarPlane = 2000.0;


}

CPerspectiveView::~CPerspectiveView()
{
}

BOOL CPerspectiveView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return COpenGLWnd::PreCreateWindow(cs);
}

// CPerspectiveView drawing

void CPerspectiveView::OnDraw(CDC* pDC)
{
	CMFC_Joe3DExporterDoc* pDoc = reinterpret_cast<CMFC_Joe3DExporterDoc *>( GetDocument( ) );
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;
	COpenGLWnd::SetContext( );


	RenderScene( );
	SwapGLBuffers( );
}


// CPerspectiveView printing

BOOL CPerspectiveView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CPerspectiveView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CPerspectiveView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}


// CPerspectiveView diagnostics

#ifdef _DEBUG
void CPerspectiveView::AssertValid() const
{
	COpenGLWnd::AssertValid();
}

void CPerspectiveView::Dump(CDumpContext& dc) const
{
	COpenGLWnd::Dump(dc);
}

CMFC_Joe3DExporterDoc* CPerspectiveView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMFC_Joe3DExporterDoc)));
	return (CMFC_Joe3DExporterDoc*)m_pDocument;
}
#endif //_DEBUG

void CPerspectiveView::RenderScene( )
{
	if( m_RenderScene != NULL )
	{
		//glEnable(GL_CULL_FACE);
		
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
		glEnable( GL_CULL_FACE );
		glLoadIdentity( );
		//SetGLConfig( );
		// move around
		glScalef( m_zoom, m_zoom, m_zoom );
		glRotatef( m_xRotation, 1.0f, 0.0f, 0.0f );
		glRotatef( m_yRotation, 0.0f, 1.0f, 0.0f );
		glRotatef( m_zRotation, 0.0f, 1.0f, 0.0f );
		glTranslatef( m_xTranslation, m_yTranslation, m_zTranslation );
		//DrawGrid( 24 );
		COpenGLWnd::RenderScene( );
		glDisable( GL_CULL_FACE );
	}
	else
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
}


void CPerspectiveView::OnSize(UINT nType, int cx, int cy)
{
	COpenGLWnd::OnSize(nType, cx, cy);

	// TODO: Add your message handler code here
	if ( 0 >= cx || 0 >= cy || nType == SIZE_MINIMIZED )
		return;

	// Change the orthographic viewing volume to
	// reflect the new dimensions of the window
	// and the zoom and position of the viewport.
	SetContext( );
	glViewport( 0, 0, cx, cy );
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity( );

	if( cx <= cy )
		m_dAspect = (float) cy / (float) cx;
	else
		m_dAspect = (float) cx / (float) cy;
	gluPerspective( 60.0, m_dAspect, m_dNearPlane, m_dFarPlane ); 
	glMatrixMode( GL_MODELVIEW );
}


void CPerspectiveView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{


	CMFC_Joe3DExporterDoc *pDoc = CMFC_Joe3DExporterDoc::getDoc( );
	pDoc->UpdateAllViews( NULL, UPD_OPENGL );

	COpenGLWnd::OnKeyDown(nChar, nRepCnt, nFlags);
}

void CPerspectiveView::OnMouseMove(UINT nFlags, CPoint point)
{
	CRect cr;
	GetClientRect( &cr );

	//if( nFlags & MK_CONTROL )
	{
		if( nFlags & MK_LBUTTON ) // rotate
		{
			CPoint ptRotate = m_ptLeftDownPos - point;
			m_ptLeftDownPos = point;
			m_xRotation -= ptRotate.y / 3.0f;
			m_zRotation -= ptRotate.x / 3.0f;
			InvalidateRect( cr, false );
		}
		else 
		if( nFlags & MK_RBUTTON ) // zoom 
		{
			CPoint ptZoom = m_ptLeftDownPos - point;
			m_ptLeftDownPos = point;
			m_zoom += ptZoom.y * 0.005f;
			//m_yTranslation -= ptZoom.y * 0.5f;
			//if( m_yTranslation >= 0.0f ) m_yTranslation = 0.0f;

			InvalidateRect( cr, false );
		}
		else if( nFlags & MK_MBUTTON ) // pan
		{
			CPoint ptPan = m_ptLeftDownPos - point;
			m_ptLeftDownPos = point;
			m_xTranslation -= ptPan.x * abs(m_zoom);
			m_zTranslation -= ptPan.y * abs(m_zoom);

			InvalidateRect( cr, false );
		}
	}
	
	
	COpenGLWnd::OnMouseMove(nFlags, point);
}

// CPerspectiveView message handlers

BOOL CPerspectiveView::OnEraseBkgnd(CDC* pDC)
{
	/*
	 *   Avoid erasing the background as it will 
	 *	 cause flickering...
	 */
	return COpenGLWnd::OnEraseBkgnd(pDC);
}

void CPerspectiveView::OnLButtonDown(UINT nFlags, CPoint point)
{
	m_ptLeftDownPos = point;
	COpenGLWnd::OnLButtonDown(nFlags, point);
}

void CPerspectiveView::OnMButtonDown(UINT nFlags, CPoint point)
{
	m_ptLeftDownPos = point;
	COpenGLWnd::OnMButtonDown(nFlags, point);
}

void CPerspectiveView::OnRButtonDown(UINT nFlags, CPoint point)
{
	m_ptLeftDownPos = point;
	COpenGLWnd::OnRButtonDown(nFlags, point);
}


bool CPerspectiveView::InitOpenGL( )
{
	COpenGLWnd::InitOpenGL( );
	return true;
}
void CPerspectiveView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint)
{
	switch( lHint )
	{
		case UPD_OPENGL:
		{
			CRect cr;
			GetClientRect( &cr );
			InvalidateRect( cr, false );
			break;
		}
		default:
			break;

	}
	
}


