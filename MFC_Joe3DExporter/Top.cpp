// Top.cpp : implementation file
//

#include "stdafx.h"
#include "MFC_Joe3DExporter.h"
#include "Top.h"
#include <list>

using namespace std;
//using namespace Game;

// CTop

IMPLEMENT_DYNCREATE(CTop, COrthographicView)

CTop::CTop()
: COrthographicView( )
{
	m_zoom = 0.025f;
}

CTop::~CTop()
{
}

BEGIN_MESSAGE_MAP(CTop, COrthographicView)
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_KEYDOWN()
END_MESSAGE_MAP()


// CTop drawing

void CTop::OnDraw(CDC* pDC)
{
	CMFC_Joe3DExporterDoc* pDoc = reinterpret_cast<CMFC_Joe3DExporterDoc *>( GetDocument( ) );
	COpenGLWnd::SetContext( );
	glLoadIdentity( );

	glRotatef( 90.0f, 1.0f, 0.0f, 0.0f );
	//glDepthMask( GL_FALSE );
	COrthographicView::OnDraw( pDC );
	//glDepthMask( GL_TRUE );
}


// CTop diagnostics

#ifdef _DEBUG
void CTop::AssertValid() const
{
	COrthographicView::AssertValid();
}

#ifndef _WIN32_WCE
void CTop::Dump(CDumpContext& dc) const
{
	COrthographicView::Dump(dc);
}
#endif
#endif //_DEBUG


// CTop message handlers

void CTop::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint )
{
	switch( lHint )
	{
		case UPD_OPENGL:
		{
			CRect cr;
			GetClientRect( &cr );
			InvalidateRect( cr, false );
			break;
		}
		default:
			break;
	}

	COrthographicView::OnUpdate( pSender, lHint, pHint );
}

void CTop::OnLButtonDown(UINT nFlags, CPoint point)
{
	CMFC_Joe3DExporterDoc *pDoc = CMFC_Joe3DExporterDoc::getDoc( );


	COrthographicView::OnLButtonDown(nFlags, point);
}

void CTop::OnMouseMove(UINT nFlags, CPoint point)
{
	CMFC_Joe3DExporterDoc *pDoc = CMFC_Joe3DExporterDoc::getDoc( );

	CRect cr;
	GetClientRect( &cr );
	static CPoint oldPoint;

	if( nFlags & MK_CONTROL )
	{
		if( nFlags & MK_RBUTTON ) // zoom 
		{
			CPoint ptZoom = m_ptLeftDownPos - point;
			m_ptLeftDownPos = point;
			m_zoom += ptZoom.y * 0.005f;
			InvalidateRect(cr, false);
		}
		else if( nFlags & MK_MBUTTON ) // pan
		{
			CPoint ptPan = m_ptLeftDownPos - point;
			m_ptLeftDownPos = point;
			m_xTranslation -= ptPan.x * (1.0f / m_zoom) * 0.001f;
			m_zTranslation -= ptPan.y * (1.0f / m_zoom) * 0.001f;
			InvalidateRect(cr, false);
		}
	}
	

	COrthographicView::OnMouseMove(nFlags, point);
}

void CTop::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	CMFC_Joe3DExporterDoc *pDoc = CMFC_Joe3DExporterDoc::getDoc( );
	pDoc->UpdateAllViews( NULL, UPD_OPENGL );

	COrthographicView::OnKeyDown(nChar, nRepCnt, nFlags);
}


