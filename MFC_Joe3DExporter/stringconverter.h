#pragma once
#ifndef _STRINGCONVERTER_H_
#define _STRINGCONVERTER_H_
/*
 *	StringConverter.h
 *
 *	A function object to convert wchar to char and char to wchar.
 */
#include <windows.h>

class CStringConverter
{
  public:
	CStringConverter( bool toWideChar /*= false*/ );
	~CStringConverter( );

	CHAR *operator()( LPCWSTR wcString );
	WCHAR *operator()( LPCSTR string );
	
	template <class T>
	void freeMemory( T *mem )
	{
		delete [] mem;
		m_bMemoryFreed = true;
	}

  protected:
	bool m_bConvertToWideChars;
	bool m_bMemoryFreed;
};


#endif