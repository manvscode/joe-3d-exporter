// MFC_Joe3DExporterDoc.cpp : implementation of the CMFC_Joe3DExporterDoc class
//

#include "stdafx.h"
#include "MFC_Joe3DExporter.h"
#include "../Joe3D/Joe3DModel.h"
#include "MFC_Joe3DExporterDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMFC_Joe3DExporterDoc

IMPLEMENT_DYNCREATE(CMFC_Joe3DExporterDoc, CDocument)

BEGIN_MESSAGE_MAP(CMFC_Joe3DExporterDoc, CDocument)
END_MESSAGE_MAP()


CMFC_Joe3DExporterDoc *CMFC_Joe3DExporterDoc::pInstance = NULL;

// CMFC_Joe3DExporterDoc construction/destruction

CMFC_Joe3DExporterDoc::CMFC_Joe3DExporterDoc( )
{
	pInstance = this;

}

CMFC_Joe3DExporterDoc *CMFC_Joe3DExporterDoc::getDoc( )
{
	if( !pInstance )
		pInstance = new CMFC_Joe3DExporterDoc( );
	return pInstance;
}

CMFC_Joe3DExporterDoc::~CMFC_Joe3DExporterDoc()
{
}

BOOL CMFC_Joe3DExporterDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}




// CMFC_Joe3DExporterDoc serialization

//void CMFC_Joe3DExporterDoc::Serialize(CArchive& ar)
//{
//	if (ar.IsStoring())
//	{
//		// TODO: add storing code here
//	}
//	else
//	{
//		// TODO: add loading code here
//	}
//}



Joe3D::CJoe3DModel *CMFC_Joe3DExporterDoc::getModel( )
{
	return &m_Model;
}


void CMFC_Joe3DExporterDoc::setFilename( string filename )
{
	m_Filename = filename;
}

string CMFC_Joe3DExporterDoc::getFilename( ) const
{
	return m_Filename;
}

// CMFC_Joe3DExporterDoc diagnostics

#ifdef _DEBUG
void CMFC_Joe3DExporterDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMFC_Joe3DExporterDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CMFC_Joe3DExporterDoc commands
