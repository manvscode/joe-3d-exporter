// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "MFC_Joe3DExporter.h"

#include "MainFrm.h"
#include "Side.h"
#include "Top.h"
#include "Front.h"
#include "PerspectiveView.h"
#include "GLRCSharer.h"
#include "stringconverter.h"
#include <maya/MLibrary.h>
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_COMMAND(ID_FILE_OPEN, &CMainFrame::OnFileOpen)
	ON_COMMAND(ID_FILE_NEW, &CMainFrame::OnFileNew)
	ON_COMMAND(ID_FILE_SAVE, &CMainFrame::OnFileSave)
	ON_COMMAND(ID_FILE_SAVE_AS, &CMainFrame::OnFileSaveAs)
	ON_COMMAND(ID_FILE_IMPORT, &CMainFrame::OnFileImport)
	ON_WM_CLOSE()
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};


// CMainFrame construction/destruction

CMainFrame::CMainFrame()
: m_initSplitters(FALSE)
{
	// TODO: add member initialization code here
}

CMainFrame::~CMainFrame()
{
}


int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	// TODO: Remove this if you don't want tool tips
	// TODO: Delete these three lines if you don't want the toolbar to be dockable
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);

	return 0;
}


BOOL CMainFrame::OnCreateClient(LPCREATESTRUCT /*lpcs*/,
	CCreateContext* pContext)
{
	CRect cr;
	GetClientRect( &cr );



	if( !m_viewportSplitter.CreateStatic( this, 2, 2 ) )
	{
		MessageBox( _T("Error setting up splitter frames!"), _T("Init Error!"), MB_OK | MB_ICONERROR );
		return FALSE;
	}

	// Top viewport
	if( !m_viewportSplitter.CreateView( 0, 0, RUNTIME_CLASS(CTop),
		CSize( cr.Width() / 2, cr.Height() / 2 ), pContext ) )
	{
		MessageBox( _T("Error setting up splitter frames!"), _T("Init Error!"),
			MB_OK | MB_ICONERROR );
		return FALSE;
	}

	// Front viewport
	if( !m_viewportSplitter.CreateView( 0, 1, RUNTIME_CLASS(CFront),
		CSize( cr.Width()/ 2, cr.Height() / 2 ), pContext ) )
	{
		MessageBox( _T("Error setting up splitter frames!"), _T("Init Error!"),
			MB_OK | MB_ICONERROR );
		return FALSE;
	}

	// Side viewport
	if( !m_viewportSplitter.CreateView( 1, 0, RUNTIME_CLASS(CSide),
		CSize( cr.Width() / 2, cr.Height() / 2 ), pContext ) )
	{
		MessageBox( _T("Error setting up splitter frames!"), _T("Init Error!"),
			MB_OK | MB_ICONERROR );
		return FALSE;
	}

	// Perspective viewport
	if( !m_viewportSplitter.CreateView( 1, 1, RUNTIME_CLASS(CPerspectiveView),
		CSize( cr.Width()/ 2, cr.Height() / 2 ), pContext ) )
	{
		MessageBox( _T("Error setting up splitter frames!"), _T("Init Error!"),
			MB_OK | MB_ICONERROR );
		return FALSE;
	}

	// By this time, this object has instantiated the shared RC's and
	// is no longer needed...
	CGLRCSharer *pRCSharer = CGLRCSharer::getInstance();
	delete pRCSharer;

	
	m_initSplitters = true;
	return TRUE;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return TRUE;
}


// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG


// CMainFrame message handlers




void CMainFrame::OnSize(UINT nType, int cx, int cy)
{
	CFrameWnd::OnSize(nType, cx, cy);
	CRect cr;

	if(  m_initSplitters && nType != SIZE_MINIMIZED )
	{
		m_viewportSplitter.GetClientRect( &cr );
		m_viewportSplitter.SetRowInfo( 0, cy / 2, 0 );
		m_viewportSplitter.SetRowInfo( 1, cy / 2, 0 );
		m_viewportSplitter.SetColumnInfo( 0, cx / 2, 0 );
		m_viewportSplitter.SetColumnInfo( 1, cx / 2, 0 );


		m_viewportSplitter.RecalcLayout( );
	}
}

void CMainFrame::OnFileOpen()
{
	CMFC_Joe3DExporterDoc *pDoc = CMFC_Joe3DExporterDoc::getDoc( );
	CFileDialog oDlg( TRUE, _T("Joe3D"), _T(""), OFN_EXPLORER, _T("Joe3D Files (*.Joe3D)|*.Joe3D|All Files (*.*)|*.*||"), this );
	CStringConverter conv( false );

	if( oDlg.DoModal( ) == IDOK )
	{
		CString ext = oDlg.GetFileExt( );
		CString filename = oDlg.GetFileName( );
		char *theFile = conv( filename.GetString( ) );
		string file(theFile);


		Joe3D::CJoe3DFileIO fileIn( file, Joe3D::CJoe3DFileIO::JOE3D_IN );
		fileIn.read( *pDoc->getModel( ) );

		pDoc->setFilename( file );

		conv.freeMemory( theFile );
		pDoc->UpdateAllViews( NULL, UPD_OPENGL );
	}
}

void CMainFrame::OnFileNew()
{
	CMFC_Joe3DExporterDoc *pDoc = CMFC_Joe3DExporterDoc::getDoc( );
	Joe3D::CJoe3DModel *pModel = pDoc->getModel( );
	pModel->clear( );
	pDoc->UpdateAllViews( NULL, UPD_OPENGL );
}

void CMainFrame::OnFileSave()
{
	CMFC_Joe3DExporterDoc *pDoc = CMFC_Joe3DExporterDoc::getDoc( );
	string &file = pDoc->getFilename( );
	Joe3D::CJoe3DFileIO fileOut( file, Joe3D::CJoe3DFileIO::JOE3D_OUT );

	fileOut.write( *pDoc->getModel( ) );
}

void CMainFrame::OnFileSaveAs()
{
	CMFC_Joe3DExporterDoc *pDoc = CMFC_Joe3DExporterDoc::getDoc( );
	CFileDialog sDlg( FALSE, _T("Joe3D"), _T(""), OFN_EXPLORER, _T("Joe3D Files (*.Joe3D)|*.Joe3D|All Files (*.*)|*.*||"), this );
	CStringConverter conv( false );

	if( sDlg.DoModal( ) == IDOK )
	{
		CString ext = sDlg.GetFileExt( );
		CString filename = sDlg.GetFileName( );
		char *theFile = conv( filename.GetString( ) );
		string file(theFile);


		Joe3D::CJoe3DFileIO fileOut( file, Joe3D::CJoe3DFileIO::JOE3D_OUT );
		fileOut.write( *pDoc->getModel( ) );
		conv.freeMemory( theFile );
		//pDoc->UpdateAllViews( NULL, UPD_OPENGL );
	}
}

void CMainFrame::OnFileImport()
{
	CMFC_Joe3DExporterDoc *pDoc = CMFC_Joe3DExporterDoc::getDoc( );
	CFileDialog sDlg( TRUE, _T("mb"), _T(""), OFN_EXPLORER, _T("Maya Files (*.mb, *.ma)|*.mb|All Files (*.*)|*.*||"), this );
	CStringConverter conv( false );

	if( sDlg.DoModal( ) == IDOK )
	{
		CString ext = sDlg.GetFileExt( );
		CString filename = sDlg.GetFileName( );
		char *theFile = conv( filename.GetString( ) );
		string file(theFile);
		
		Joe3D::CJoe3DModel &model = *pDoc->getModel( );
		model.clear( );
		Joe3D::CJoe3DFileIO::import( file, model );

		conv.freeMemory( theFile );
		//pDoc->UpdateAllViews( NULL, UPD_OPENGL );
	}
}

void CMainFrame::OnClose()
{
	// clean up Maya...
	//MLibrary::cleanup( 0 );  //

	CFrameWnd::OnClose();
}
