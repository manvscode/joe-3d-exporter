//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by MFC_Joe3DExporter.rc
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDR_MAINFRAME                   128
#define IDR_Joe3DTYPE                   129
#define IDD_PROGRESS_DIALOG             130
#define IDB_COOLBUTTON1                 141
#define IDB_COOLBUTTON2                 142
#define IDB_COOLBUTTON3                 143
#define IDC_PROGRESS                    1000
#define ID_FILE_EXPORT                  32771
#define ID_FILE_IMPORT                  32772

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32773
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
