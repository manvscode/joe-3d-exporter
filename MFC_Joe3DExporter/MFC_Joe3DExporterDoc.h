// MFC_Joe3DExporterDoc.h : interface of the CMFC_Joe3DExporterDoc class
//


#pragma once
#include <string>
#include "../Joe3D/Joe3DModel.h"
using namespace std;



class CMFC_Joe3DExporterDoc : public CDocument
{
protected: // create from serialization only
	CMFC_Joe3DExporterDoc();
	DECLARE_DYNCREATE(CMFC_Joe3DExporterDoc)


	static CMFC_Joe3DExporterDoc *pInstance;
	Joe3D::CJoe3DModel m_Model;

	std::string m_Filename;

// Attributes
public:

// Operations
public:
	static CMFC_Joe3DExporterDoc *getDoc( );
	Joe3D::CJoe3DModel *getModel( );
	void setFilename( string filename );
	string getFilename( ) const;
// Overrides
public:
	virtual BOOL OnNewDocument();
	//virtual void Serialize(CArchive& ar);

// Implementation
public:
	virtual ~CMFC_Joe3DExporterDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
};


