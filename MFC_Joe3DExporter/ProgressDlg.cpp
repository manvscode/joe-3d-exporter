// ProgressDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MFC_Joe3DExporter.h"
#include "ProgressDlg.h"


// CProgressDlg dialog

IMPLEMENT_DYNAMIC(CProgressDlg, CDialog)

CProgressDlg::CProgressDlg( int nMin, int nMax, int nStep, CWnd* pParent /*=NULL*/ )
	: CDialog(CProgressDlg::IDD, pParent)//, min(nMin), max(nMax)
{
	min = nMin;
	max = nMax;
	step = nStep;
}

CProgressDlg::~CProgressDlg( )
{
}

void CProgressDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PROGRESS, m_ProgressBar);
}


BEGIN_MESSAGE_MAP(CProgressDlg, CDialog)
//	ON_WM_CREATE()
END_MESSAGE_MAP()


// CProgressDlg message handlers
void CProgressDlg::stepIt( )
{
	m_ProgressBar.StepIt( );
}

void CProgressDlg::setProgress( int position )
{
	ASSERT( position <= max );
	ASSERT( position >= m_ProgressBar.GetPos( ) );
	m_ProgressBar.SetPos( position );
}

bool CProgressDlg::isDone( )
{
	bool done = m_ProgressBar.GetPos( ) == max;
	return done;
}


BOOL CProgressDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	CenterWindow( );
	// TODO:  Add extra initialization here
	m_ProgressBar.SetRange32( min, max );
	//m_ProgressBar.SetBkColor( RGB( 10, 10, 10 ) );
	m_ProgressBar.SetStep( step );
	m_StatusText = "processing...";
	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}
