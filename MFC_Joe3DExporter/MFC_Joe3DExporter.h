// MFC_Joe3DExporter.h : main header file for the MFC_Joe3DExporter application
//
#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"       // main symbols


// CMFC_Joe3DExporterApp:
// See MFC_Joe3DExporter.cpp for the implementation of this class
//

class CMFC_Joe3DExporterApp : public CWinApp
{
public:
	CMFC_Joe3DExporterApp();


// Overrides
public:
	virtual BOOL InitInstance();

// Implementation
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
//	virtual int ExitInstance();
//	virtual BOOL InitApplication();
//	virtual void OnFinalRelease();
	virtual void OnFinalRelease();
};

extern CMFC_Joe3DExporterApp theApp;

typedef enum tagUpdateType {
	UPD_DEFAULT = 0,
	UPD_OPENGL = 1
} UpdateType;