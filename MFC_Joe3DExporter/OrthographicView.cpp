// OrthogonalView.cpp : implementation file
//

#include "stdafx.h"
#include "MFC_Joe3DExporter.h"
#include "OrthographicView.h"

// COrthographicView

IMPLEMENT_DYNCREATE(COrthographicView, COpenGLWnd)

COrthographicView::COrthographicView()
: COpenGLWnd( )
{
	m_zoom = 0.05f;
	m_xTranslation = 0.0;
	m_yTranslation = 0.0;
	m_zTranslation = -5.0;
	m_xRotation = 0.0;
	m_yRotation = 0.0;
	m_zRotation = 0.0;
	m_dAspect = 1.0;
	m_dNearPlane = 1.0;
	m_dFarPlane = 100.0;
}

COrthographicView::~COrthographicView()
{
}

BEGIN_MESSAGE_MAP(COrthographicView, COpenGLWnd)
	ON_WM_SIZE()
	ON_WM_LBUTTONDOWN()
	ON_WM_MBUTTONDOWN()
	ON_WM_RBUTTONDOWN()
END_MESSAGE_MAP()


// COrthographicView drawing

void COrthographicView::OnDraw(CDC* pDC)
{
	CDocument* pDoc = GetDocument();

	RenderScene( );
	SwapGLBuffers( );
}


void COrthographicView::SetFrustum( )
{
	//glMatrixMode(GL_PROJECTION);
	//glLoadIdentity();
	//glOrtho( -1.0, 1.0, -1.0, 1.0, -1000.0, 1000.0 );
	//glMatrixMode(GL_MODELVIEW);
}

bool COrthographicView::InitOpenGL( )
{
	COpenGLWnd::InitOpenGL( );
	return true;
}

// COrthographicView diagnostics

#ifdef _DEBUG
void COrthographicView::AssertValid() const
{
	COpenGLWnd::AssertValid();
}

#ifndef _WIN32_WCE
void COrthographicView::Dump(CDumpContext& dc) const
{
	COpenGLWnd::Dump(dc);
}
#endif
#endif //_DEBUG


// COrthographicView message handlers
void COrthographicView::RenderScene( )
{
	if( m_RenderScene != NULL )
	{
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
		//SetGLConfig( );
		// move around
		glScalef( m_zoom, m_zoom, m_zoom );
		glTranslatef( m_xTranslation, m_yTranslation, m_zTranslation );
		
		//DrawGrid( 24 );
		glEnable( GL_TEXTURE_2D );
		COpenGLWnd::RenderScene( );
	}
	else
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
}
void COrthographicView::OnSize(UINT nType, int cx, int cy)
{
	SetContext( );

	glViewport( 0, 0, cx, cy );
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity( );

	float aspect = (float) cx / (float) cy;

	glOrtho( aspect, aspect, -1.0, 1.0, -1000.0, 1000.0 );
	glMatrixMode(GL_MODELVIEW);

}

void COrthographicView::OnLButtonDown(UINT nFlags, CPoint point)
{
	m_ptLeftDownPos = point;
	COpenGLWnd::OnLButtonDown(nFlags, point);
}

void COrthographicView::OnMButtonDown(UINT nFlags, CPoint point)
{
	m_ptLeftDownPos = point;
	COpenGLWnd::OnMButtonDown(nFlags, point);
}

void COrthographicView::OnRButtonDown(UINT nFlags, CPoint point)
{
	m_ptLeftDownPos = point;
	COpenGLWnd::OnRButtonDown(nFlags, point);
}

void COrthographicView::OnInitialUpdate()
{
	COpenGLWnd::OnInitialUpdate();

}

