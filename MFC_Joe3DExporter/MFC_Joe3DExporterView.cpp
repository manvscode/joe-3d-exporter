// MFC_Joe3DExporterView.cpp : implementation of the CMFC_Joe3DExporterView class
//

#include "stdafx.h"
#include "MFC_Joe3DExporter.h"

#include "MFC_Joe3DExporterDoc.h"
#include "MFC_Joe3DExporterView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMFC_Joe3DExporterView

IMPLEMENT_DYNCREATE(CMFC_Joe3DExporterView, CView)

BEGIN_MESSAGE_MAP(CMFC_Joe3DExporterView, CView)
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
END_MESSAGE_MAP()

// CMFC_Joe3DExporterView construction/destruction

CMFC_Joe3DExporterView::CMFC_Joe3DExporterView()
{
	// TODO: add construction code here

}

CMFC_Joe3DExporterView::~CMFC_Joe3DExporterView()
{
}

BOOL CMFC_Joe3DExporterView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

// CMFC_Joe3DExporterView drawing

void CMFC_Joe3DExporterView::OnDraw(CDC* /*pDC*/)
{
	CMFC_Joe3DExporterDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: add draw code for native data here
}


// CMFC_Joe3DExporterView printing

BOOL CMFC_Joe3DExporterView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CMFC_Joe3DExporterView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CMFC_Joe3DExporterView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}


// CMFC_Joe3DExporterView diagnostics

#ifdef _DEBUG
void CMFC_Joe3DExporterView::AssertValid() const
{
	CView::AssertValid();
}

void CMFC_Joe3DExporterView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CMFC_Joe3DExporterDoc* CMFC_Joe3DExporterView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMFC_Joe3DExporterDoc)));
	return (CMFC_Joe3DExporterDoc*)m_pDocument;
}
#endif //_DEBUG


// CMFC_Joe3DExporterView message handlers
