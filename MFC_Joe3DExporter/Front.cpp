// Front.cpp : implementation file
//

#include "stdafx.h"
#include "MFC_Joe3DExporter.h"
#include "Front.h"


// CFront

IMPLEMENT_DYNCREATE(CFront, COrthographicView)

CFront::CFront()
: COrthographicView( )
{

}

CFront::~CFront()
{
}

BEGIN_MESSAGE_MAP(CFront, COrthographicView)
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()


// CFront drawing

void CFront::OnDraw(CDC* pDC)
{
	CMFC_Joe3DExporterDoc* pDoc = reinterpret_cast<CMFC_Joe3DExporterDoc *>( GetDocument( ) );
	SetContext( );
	glLoadIdentity( );

	glDisable( GL_CULL_FACE );
	COrthographicView::OnDraw( pDC );
	glEnable( GL_CULL_FACE );
}


// CFront diagnostics

#ifdef _DEBUG
void CFront::AssertValid() const
{
	COrthographicView::AssertValid();
}

#ifndef _WIN32_WCE
void CFront::Dump(CDumpContext& dc) const
{
	COrthographicView::Dump(dc);
}
#endif
#endif //_DEBUG


// CFront message handlers

void CFront::OnMouseMove(UINT nFlags, CPoint point)
{
	CRect cr;
	GetClientRect( &cr );

	if( nFlags & MK_CONTROL )
	{
		if( nFlags & MK_RBUTTON ) // zoom 
		{
			CPoint ptZoom = m_ptLeftDownPos - point;
			m_ptLeftDownPos = point;
			m_zoom += ptZoom.y * 0.005f;
			InvalidateRect(cr, false);
		}
		else if( nFlags & MK_MBUTTON ) // pan
		{
			CPoint ptPan = m_ptLeftDownPos - point;
			m_ptLeftDownPos = point;
			m_xTranslation -= ptPan.x * (1.0f / m_zoom) * 0.001f;
			m_yTranslation += ptPan.y * (1.0f / m_zoom) * 0.001f;
			InvalidateRect(cr, false);
		}
	}

	COrthographicView::OnMouseMove(nFlags, point);
}

void CFront::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint)
{
	switch( lHint )
	{
		case UPD_OPENGL:
		{
			CRect cr;
			GetClientRect( &cr );
			InvalidateRect( cr, false );
			break;
		}
		default:
			break;
	}
}
