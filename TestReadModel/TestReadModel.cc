/*
 *	TestReadModel.cc
 * 
 *	Test Driver
 *
 *	Coded by Joseph A. Marrero
 *	5/11/2007
 */
#include <GL/glew.h>
#include <GL/glut.h>
#include <cassert>
#include <iostream>
#include "TestReadModel.h"



using namespace std;

int main( int argc, char *argv[] )
{
	glutInit( &argc, argv );
	glutInitDisplayMode( GLUT_RGBA | GLUT_DOUBLE );
	glutInitWindowSize( 640, 480 );
	
	glutCreateWindow( "TestReadModel" );
	//glutFullScreen( );

	GLenum err = glewInit( );
	if( GLEW_OK != err )
	{
		/* Problem: glewInit failed, something is seriously wrong. */
		cerr << "Error: " << glewGetErrorString( err ) << endl;
		return -1;
	}

	glutDisplayFunc( render );
	glutReshapeFunc( resize );
	glutKeyboardFunc( keyboard_keypress );
	glutIdleFunc( idle );

	initialize( );
	glutMainLoop( );
	deinitialize( );

	return 0;
}


void initialize( )
{
	glEnable( GL_ALPHA_TEST );
	
	glEnable( GL_POINT_SMOOTH );
	glEnable( GL_LINE_SMOOTH );
	//glEnable( GL_POLYGON_SMOOTH );
	glHint( GL_POINT_SMOOTH_HINT, GL_NICEST );
	glHint( GL_LINE_SMOOTH_HINT, GL_NICEST );
	//glHint( GL_POLYGON_SMOOTH_HINT, GL_NICEST );
	glHint( GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST );
	
	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	

	glShadeModel( GL_SMOOTH );
	glClearDepth( 1.0f );		
	glDepthFunc( GL_LEQUAL );
	glEnable( GL_DEPTH_TEST );

	glEnable( GL_LIGHTING );
	glEnable( GL_LIGHT0 );
	glEnable( GL_LIGHT1 );

	glPointSize( 3.0f );
	glEnable( GL_TEXTURE_2D );
	glEnableClientState( GL_VERTEX_ARRAY );
	glEnableClientState( GL_NORMAL_ARRAY );
	glEnableClientState( GL_TEXTURE_COORD_ARRAY );


	glPixelStorei( GL_PACK_ALIGNMENT, 4 );
	glPixelStorei( GL_UNPACK_ALIGNMENT, 4 );


	Joe3D::CJoe3DFileIO fileIn( "../sub.joe3d", Joe3D::CJoe3DFileIO::JOE3D_IN );
	fileIn.read( theModel );
}

void deinitialize( )
{
	// TO DO: Deinitialization code goes here...
}


void render( )
{
	static float angle = 0.0f;
	static float lightDirection[] = { 0.0f, -1.0f, -0.5f, 0.0f };
	static float lightPosition[] = { 0.0f, 20.0f, 10.0f, 0.0f };
	static float lightDiffuse[] = { 0.8f, 0.8f, 0.8f, 0.0f };
	static float lightAmbient[] = { 0.2f, 0.2f, 0.3f, 0.0f };

	glClearColor( 0.8f, 0.5f, 0.5f, 0.2f );
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	glLoadIdentity( );


	glTranslatef( 0.0f, 0.0f, -30.0f );
	glRotatef( 25.0f, 1.0f, 0.0f, 0.0f );

	glLightfv( GL_LIGHT0, GL_POSITION, lightPosition );
	glLightfv( GL_LIGHT0, GL_SPOT_DIRECTION, lightDirection );
	glLightfv( GL_LIGHT0, GL_DIFFUSE, lightDiffuse );
	glLightfv( GL_LIGHT0, GL_AMBIENT, lightAmbient );

	glRotatef( angle++, 0.0f, 1.0f, 0.0f );

	if( angle >= 360.0f )
		angle = 0.0f;

	//glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
	glColor3f( 0.5f, 0.5f, 0.5f );
	theModel.render( );




	/* Write text */
	int width = glutGet((GLenum)GLUT_WINDOW_WIDTH);
	int height = glutGet((GLenum)GLUT_WINDOW_HEIGHT);

	writeText( GLUT_BITMAP_HELVETICA_18, std::string("TestReadModel"), 2, 22 );
	writeText( GLUT_BITMAP_9_BY_15, std::string("Press Q to quit."), 2, 5 );
	
	glutSwapBuffers( );
}

void resize( int width, int height )
{
	glViewport( 0, 0, width, height );
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity( );
	

	if( height == 0 )
		height = 1;
		
	gluPerspective( 45.0f, (double) width / (double) height, 1.0, 100.0 );
	glMatrixMode( GL_MODELVIEW );
}

void keyboard_keypress( unsigned char key, int x, int y )
{
	switch( key )
	{
		case 'Q':
		case 'q':
		case GLUT_KEY_F1:
		case GLUT_KEY_END:
			deinitialize( );
			exit( 0 );
			break;
		default:
			break;
	}

}

void idle( )
{ glutPostRedisplay( ); }

void writeText( void *font, std::string &text, int x, int y )
{
	int width = glutGet( (GLenum) GLUT_WINDOW_WIDTH );
	int height = glutGet( (GLenum) GLUT_WINDOW_HEIGHT );

	glDisable( GL_DEPTH_TEST );

	glPushAttrib( GL_LIGHTING_BIT | GL_TEXTURE_BIT );

	glDisable( GL_TEXTURE_2D );
	glDisable( GL_LIGHTING );

	glMatrixMode( GL_PROJECTION );
	glPushMatrix( );
	glLoadIdentity( );	
	glOrtho( 0, width, 0, height, 1.0, 10.0 );

	glMatrixMode( GL_MODELVIEW );
	glPushMatrix( );
	glLoadIdentity( );
	glColor3f( 1.0f, 1.0f, 1.0f );
	glTranslatef( 0.0f, 0.0f, -1.0f );
	glRasterPos2i( x, y );

	for( unsigned int i = 0; i < text.size( ); i++ )
		glutBitmapCharacter( font, text[ i ] );

	glPopMatrix( );
	glMatrixMode( GL_PROJECTION );
	glPopMatrix( );
	glMatrixMode( GL_MODELVIEW );

	//glEnable( GL_LIGHTING );

	//glEnable( GL_TEXTURE_2D );
	glPopAttrib( );
	glEnable( GL_DEPTH_TEST );
}
