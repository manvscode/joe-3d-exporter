#ifndef _TESTREADMODEL_H_
#define _TESTREADMODEL_H_
/*
 *	TestReadModel.h
 *
 *	Test Driver
 *
 *	Coded by Joseph A. Marrero
 *	5/11/2007
 */
#include <string>
#include "../Joe3D/Joe3DModel.h"
using namespace std;


void initialize( );
void deinitialize( );

void render( );
void resize( int width, int height );
void keyboard_keypress( unsigned char key, int x, int y );
void idle( );
void writeText( void *font, std::string &text, int x, int y );

Joe3D::CJoe3DModel theModel;
#endif
