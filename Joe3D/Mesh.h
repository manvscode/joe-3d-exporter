#ifndef _MESH_H_
#define _MESH_H_
/*
 *	Mesh.h
 *
 *	A generic mesh class. This class is a container of the 
 *	geometry and texture data.
 *
 *	Coded by Joseph A. Marrero
 *	5/11/07
 */
#include <Vector3.h>
#include <cassert>
#include <vector>


namespace Joe3D {


class Mesh
{
  public:
	#pragma pack(push,1)
	struct Vertex {
		union {
			struct {
				float x, y, z;
			};
			float vertex[ 3 ];
		};
	};

	struct Normal {
		union {
			struct {
				float nx, ny, nz;
			};
			float normal[ 3 ];
		};
	};

	struct UV {
		union {
			struct {
				float u; float v;
			};
			float texture[ 2 ];
		};
	};
	#pragma pack(pop)
	
	enum MaterialType {
		MAT_AMBIENT, MAT_DIFFUSE, MAT_SPECULAR
	};

	typedef unsigned int                                     Index;
	typedef std::vector<Mesh::Vertex>                        Vertices;
	typedef std::vector<Mesh::Normal>                        Normals;
	typedef std::vector<Mesh::UV>                            UVs;
	typedef std::vector< std::pair<Mesh::Index, Mesh::UVs> > TextureUV_Map; // for multi-texturing; key is the textureId and value is the texture coords
	typedef std::vector<Mesh::Index>                         IndexCollection;

  protected:
	std::string   m_Name;
	Vertices      m_Vertices;
	Normals       m_Normals;
	TextureUV_Map m_UVs; // for multi-texturing; key is the textureId and value is the texture coords
	unsigned int  m_NumberOfUVSets;


	IndexCollection m_TriangleList;

	Math::Vector3 m_Translation, m_Rotation, m_Scale;

	float m_AmbientMaterial[ 4 ];
	float m_DiffuseMaterial[ 4 ];
	float m_SpecularMaterial[ 4 ];
	float m_Shininess;

  public:
	Mesh( );
	virtual ~Mesh( );
	void setName( const std::string &name );
	std::string &name( );
	const std::string &name( ) const;

	void setVertices( const Vertices &vertices );
	void setNormals( const Normals &normals );
	void setUVs( unsigned int textureSet, UVs &uvs );
	void setTexture( unsigned int textureSet, unsigned int textureId );
	void addUVs( UVs &uvs, unsigned int textureId = 0 );
	void setTriangleIndexList( const IndexCollection &triangleIndexList );

	Vertices &vertices( );
	const Vertices &vertices( ) const;
	Normals &normals( );
	const Normals &normals( ) const;
	UVs &getUVs( unsigned int textureSet );
	const UVs &getUVs( unsigned int textureSet ) const;
	unsigned int numberOfUVTextureSets( ) const;
	IndexCollection &triangleIndexList( );
	const IndexCollection &triangleIndexList( ) const;
	unsigned int numberOfTriangles( ) const;

	void setMaterial( MaterialType materialType, float *material );
	float *getMaterial( MaterialType materialType );
	const float *getMaterial( MaterialType materialType ) const;
	void setShininess( float s );
	float getShininess( ) const;

	//void addVertex( Vertex &v );
	Vertex &operator[]( int idx );
	Vertex &operator[]( unsigned int idx );
	void render( );
};


inline Mesh::Vertices &Mesh::vertices( )
{ 
	return m_Vertices;
}

inline const Mesh::Vertices &Mesh::vertices( ) const
{
	return m_Vertices;
}

inline Mesh::Normals &Mesh::normals( )
{
	return m_Normals;
}

inline const Mesh::Normals &Mesh::normals( ) const
{ 
	return m_Normals;
}

inline Mesh::UVs &Mesh::getUVs( unsigned int textureSet )
{
	assert( textureSet >= 0  && textureSet <= m_NumberOfUVSets );
	return m_UVs[ textureSet ].second;
}

inline const Mesh::UVs &Mesh::getUVs( unsigned int textureSet ) const
{
	assert( textureSet >= 0  && textureSet <= m_NumberOfUVSets );
	return m_UVs[ textureSet ].second;
}

inline Mesh::IndexCollection &Mesh::triangleIndexList( )
{ 
	return m_TriangleList;
}

inline const Mesh::IndexCollection &Mesh::triangleIndexList( ) const
{ 
	return m_TriangleList; 
}






inline unsigned int Mesh::numberOfUVTextureSets( ) const
{ return m_UVs.size(); }

inline unsigned int Mesh::numberOfTriangles( ) const
{ return m_TriangleList.size( ) / 3; }

inline float Mesh::getShininess( ) const
{ return m_Shininess; }


} // end of namespace Joe3D
#endif
