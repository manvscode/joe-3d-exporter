#ifndef _JOE3DFILEIO_H_
#define _JOE3DFILEIO_H_
/*
 *	FileIO.h
 *
 *	File IO operations for Joe3D models.
 *	Coded by Joseph A. Marrero}
 */
#ifndef MLIBRARY_DONTUSE_MFC_MANIFEST
#define MLIBRARY_DONTUSE_MFC_MANIFEST
#endif
#include <maya/MObject.h>
#include <maya/MDagPath.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MPointArray.h>
#include <maya/MFloatArray.h>
#include <maya/MFloatVectorArray.h>
#include <ostream>
#include <iostream>
#include <fstream>
#include "Mesh.h"

namespace Joe3D {

class Model;

class FileIO
{
  public:
	static const int FILE_ID = 0xFEED3D; // File Marker
	enum IOMode {
		JOE3D_IN, JOE3D_OUT
	};

	FileIO( const std::string &filename, IOMode mode = JOE3D_IN );
	~FileIO( );

	/*
	 * read( ) - reads a Joe3D file and initializes a
	 *			 Model object.
	 * returns true on success; false otherwise.
	 */
	bool read( Model &model );
	/*
	 * write( ) - writes a Model object to a
	 *			  Joe3D file.
	 * returns true on success; false otherwise.
	 */
	bool write( const Model &model );
	/*
	 * import( ) - imports a Maya file to a Model
	 *			   object.
	 * returns true on success; false otherwise.
	 */
	static bool import( const std::string &filename, Model &model, bool bLoadMayaLibrary = false );

  private:
	struct ModelDescriptor {
		unsigned int modelNameLength;
		char *modelName;
		unsigned int numberOfMeshObjects;
		unsigned int numberOfTextures;
	};

	struct MeshDescriptor {
		unsigned int meshNameLength;
		unsigned int numberOfVertices;
		unsigned int numberOfNormals;
		unsigned int numberOfUVSets;
		unsigned int numberOfTriangleIndices;
	};

	struct UVDescriptor {
		unsigned int numberOfUVs;
	};

	struct TextureDescriptor {
		unsigned int textureFilenameLength;
	};

	bool readMeshData( Mesh &m );
	bool writeMeshData( const Mesh &m );
	static void buildMeshGeometry( Mesh &mesh, MObject &obj );
	static void buildMeshTextureData( Mesh &mesh, MObject &obj );
	static void buildMeshMaterialData( Model &model, Mesh &mesh, MObject &obj, MDagPath &path );
	static bool buildSurfaceShader( Mesh &mesh, MObject &obj, unsigned int &numberOfMaterialsFound );

	static bool m_bTextured;
	static bool m_bMultiTextured;

  protected:
	std::fstream m_File;
	IOMode m_IOMode;
	std::string m_Filename;
};


} // end of namespace Joe3D
#endif
