/*
 *	Mesh.cpp
 *
 *	A generic mesh class. This class is a container of the 
 *	geometry and texture data.
 *
 *	Coded by Joseph A. Marrero
 *	5/11/07
 */
#include <GL/glew.h>
#include <cassert>
#include "Mesh.h"

namespace Joe3D {

Mesh::Mesh( )
  : m_Name(),
    m_Vertices(), 
	m_Normals(), 
	m_UVs(), 
	m_NumberOfUVSets(0)
{
	memset( m_AmbientMaterial, 0, sizeof(float) * 4 );
	memset( m_DiffuseMaterial, 0, sizeof(float) * 4 );
	memset( m_SpecularMaterial, 0, sizeof(float) * 4 );
}

Mesh::~Mesh( )
{
}

std::string &Mesh::name( )
{ 
	return m_Name;
}

const std::string &Mesh::name( ) const
{ 
	return m_Name;
}

void Mesh::setName( const std::string &name )
{
	m_Name = name;
}

void Mesh::setVertices( const Mesh::Vertices &vertices )
{
	m_Vertices = vertices;
}

void Mesh::setNormals( const Mesh::Normals &normals )
{ 
	m_Normals = normals;
}

void Mesh::setUVs( unsigned int textureSet, Mesh::UVs &uvs )
{ 
	assert( textureSet >= 0  && textureSet <= m_NumberOfUVSets );
	m_UVs[ textureSet ] = std::pair<Index, Mesh::UVs>( 0, uvs );
}

void Mesh::setTexture( unsigned int textureSet, Mesh::Index textureId )
{
	assert( textureSet >= 0  && textureSet <= m_NumberOfUVSets );
	m_UVs[ textureSet ].first = textureId;
}

void Mesh::addUVs( UVs &uvs, Mesh::Index textureId )
{
	m_UVs.push_back( std::pair<Index, UVs>( textureId, uvs ) );
	m_NumberOfUVSets++;
}

void Mesh::setTriangleIndexList( const Mesh::IndexCollection &triangleIndexList )
{ 
	m_TriangleList = triangleIndexList;
}

void Mesh::setMaterial( Mesh::MaterialType materialType, float *material )
{
	assert( material != NULL );

	switch( materialType )
	{
		case MAT_AMBIENT:
			memcpy( m_AmbientMaterial, material, sizeof(float) * 4 );
			break;
		case MAT_DIFFUSE:
			memcpy( m_DiffuseMaterial, material, sizeof(float) * 4 );
			break;
		case MAT_SPECULAR:
			memcpy( m_SpecularMaterial, material, sizeof(float) * 4 );
			break;
		default:
			assert( false ); //how'd did i get here?
	}
}

float *Mesh::getMaterial( MaterialType materialType )
{
	switch( materialType )
	{
		case MAT_AMBIENT:
			return m_AmbientMaterial;
		case MAT_DIFFUSE:
			return m_DiffuseMaterial;
		case MAT_SPECULAR:
			return m_SpecularMaterial;
		default:
			return NULL;
	}
}

const float *Mesh::getMaterial( MaterialType materialType ) const
{
	switch( materialType )
	{
			case MAT_AMBIENT:
				return m_AmbientMaterial;
			case MAT_DIFFUSE:
				return m_DiffuseMaterial;
			case MAT_SPECULAR:
				return m_SpecularMaterial;
			default:
				return NULL;
	}
}

void Mesh::setShininess( float s )
{ m_Shininess = s; }

//void Mesh::addVertex( Mesh::Vertex &v )
//{ m_Vertices.push_back( v ); }

Mesh::Vertex &Mesh::operator[]( int idx )
{ return m_Vertices[ idx ]; }

Mesh::Vertex &Mesh::operator[]( unsigned int idx )
{ return m_Vertices[ idx ]; }

void Mesh::render( )
{
	glPushMatrix( );
		glTranslatef( m_Translation.X, m_Translation.Y, m_Translation.Z );

		glMaterialfv( GL_FRONT, GL_AMBIENT, m_AmbientMaterial );
		glMaterialfv( GL_FRONT, GL_DIFFUSE, m_DiffuseMaterial );
		glMaterialfv( GL_FRONT, GL_SPECULAR, m_SpecularMaterial );

		glMaterialf( GL_FRONT, GL_SHININESS, 120 );


		for( unsigned int i = 0; i < m_NumberOfUVSets; i++ )
		{
			//glActiveTexture( GL_TEXTURE0 + i );
			glBindTexture( GL_TEXTURE_2D, m_UVs[ i ].first );
			glTexCoordPointer( 2, GL_FLOAT, 0, &m_UVs[ i ].second[ 0 ] );
		}

		glVertexPointer( 3, GL_FLOAT, 0, &m_Vertices[ 0 ] );
		glNormalPointer( GL_FLOAT, 0, &m_Normals[ 0 ] );
		glIndexPointer( GL_UNSIGNED_INT, 0, &m_TriangleList[ 0 ] );


		glDrawElements( GL_TRIANGLES, m_TriangleList.size( ), GL_UNSIGNED_INT, &m_TriangleList[ 0 ] );
	glPopMatrix( );
}

} // end of namespace Joe3D
