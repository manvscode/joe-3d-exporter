#ifndef _JOE3DMODEL_H_
#define _JOE3DMODEL_H_

#include <vector>
using namespace std;
#include "Mesh.h"
#include "FileIO.h"

namespace Joe3D {

class Model
{
	friend class FileIO;

  protected:
	vector<Mesh> m_Meshes;
	string m_Name;
	vector<string> m_Textures;

  public:
	Model( );
	~Model( );

	void addMesh( const Mesh &mesh );
	void addTexture( const string &textureFilename );

	void setName( const string &name );
	string &name( );
	string name( ) const;

	void render( );
	void clear( );

	Mesh &operator[]( int idx );
	unsigned int numberOfMeshes( ) const;
	const Mesh &operator[]( int idx ) const;
};

inline unsigned int Model::numberOfMeshes( ) const
{ return m_Meshes.size( ); }


} // end of namespace Joe3D
#endif
