/*
 *	FileIO.cpp
 *
 *	File IO operations for Joe3D models.
 *	Coded by Joseph A. Marrero
 *	5/11/07
 */
#ifndef MLIBRARY_DONTUSE_MFC_MANIFEST
#define MLIBRARY_DONTUSE_MFC_MANIFEST
#endif
#include <maya/MItDag.h>
#include <maya/MItMeshPolygon.h>
#include <maya/MPlug.h>
#include <maya/MItDependencyNodes.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MFnMesh.h>
#include <maya/MDagPath.h>
#include <maya/MFnLambertShader.h>
#include <maya/MFnPhongShader.h>
#include <maya/MFnBlinnShader.h>
#include <maya/MFileIO.h>
#include <maya/MLibrary.h>
#include <maya/MGlobal.h>
#include <maya/MPointArray.h>
#include <ostream>
#include <fstream>
#include <iostream>
#include <cassert>
#include "FileIO.h"
#include "Model.h"

#ifdef WIN32
#pragma comment(lib,"Foundation.lib")
#pragma comment(lib,"OpenMaya.lib")
//#pragma comment(lib,"OpenMayaFx.lib")
//#pragma comment(lib,"Image.lib")
//#pragma comment(lib,"OpenMayaAnim.lib")
#endif

namespace Joe3D {

bool FileIO::m_bTextured = false;
bool FileIO::m_bMultiTextured = false;

FileIO::FileIO( const string &filename, IOMode mode )
: m_File(filename.c_str(), (mode == JOE3D_IN ? ios::in | ios::binary : ios::out | ios::binary)),
  m_IOMode(mode),
  m_Filename(filename)
{}

FileIO::~FileIO( )
{}


bool FileIO::read( Model &model )
{
	ModelDescriptor descriptor;

	// read the file identifier...
	int ID = 0;
	m_File.read( (char *) &ID, sizeof(int) );

	if( (ID ^ FILE_ID) != 0 )
		return false;

	// read out the file descriptor...
	m_File.read( (char *) &descriptor.modelNameLength, sizeof(unsigned int) );
	descriptor.modelName = new char[ descriptor.modelNameLength ];
	m_File.read( descriptor.modelName, sizeof(char) * descriptor.modelNameLength );
	m_File.read( (char *) &descriptor.numberOfMeshObjects, sizeof(unsigned int) );
	m_File.read( (char *) &descriptor.numberOfTextures, sizeof(unsigned int) );

	model.m_Name.assign( descriptor.modelName );
	delete [] descriptor.modelName;

	for( unsigned int i = 0; i < descriptor.numberOfMeshObjects; i++ )
	{
		Mesh m;

		if( !readMeshData( m ) ) return false;
		model.addMesh( m );
	}

	for( unsigned int i = 0; i < descriptor.numberOfTextures; i++ )
	{
		TextureDescriptor texDescriptor;
		
		m_File.read( (char *) &texDescriptor.textureFilenameLength, sizeof(unsigned int) );
		char *textureFilename = new char[ texDescriptor.textureFilenameLength ];
		m_File.read( (char *) textureFilename, sizeof(unsigned int) );
		string theTextureFilename( textureFilename );
		model.addTexture( theTextureFilename );
		delete [] textureFilename;
	}
		
	return true;
}

bool FileIO::write( const Model &model )
{
	ModelDescriptor descriptor;

	descriptor.modelNameLength = model.m_Name.length( ) + 1;
	descriptor.modelName = const_cast<char *>( &model.m_Name[ 0 ] );
	descriptor.numberOfMeshObjects = model.m_Meshes.size( );
	descriptor.numberOfTextures = model.m_Textures.size( );


	// write the file identifier...
	m_File.write( (const char *) &FILE_ID, sizeof(int) );

	// write out the file descriptor...
	m_File.write( (const char *) &descriptor.modelNameLength, sizeof(unsigned int) );
	m_File.write( descriptor.modelName, sizeof(char) * descriptor.modelNameLength );
	m_File.write( (const char *) &descriptor.numberOfMeshObjects, sizeof(unsigned int) );
	m_File.write( (const char *) &descriptor.numberOfTextures, sizeof(unsigned int) );

	for( unsigned int i = 0; i < descriptor.numberOfMeshObjects; i++ )
		if( !writeMeshData( model[ i ] ) ) return false;

	for( unsigned int i = 0; i < descriptor.numberOfTextures; i++ )
	{
		TextureDescriptor texDescriptor;
		texDescriptor.textureFilenameLength = model.m_Textures[ i ].length( );

		m_File.write( (const char *) &texDescriptor.textureFilenameLength, sizeof(unsigned int) );
		m_File.write( (const char *) model.m_Textures[ i ].c_str( ), sizeof(unsigned int) );
	}

	return true;
}

bool FileIO::readMeshData( Mesh &m )
{
	MeshDescriptor mDescriptor;


	// read out the model descriptor...
	m_File.read( (char *) &mDescriptor.meshNameLength, sizeof(unsigned int) );
	m_File.read( (char *) &mDescriptor.numberOfVertices, sizeof(unsigned int) );
	m_File.read( (char *) &mDescriptor.numberOfNormals, sizeof(unsigned int) );
	m_File.read( (char *) &mDescriptor.numberOfUVSets, sizeof(unsigned int) );
	m_File.read( (char *) &mDescriptor.numberOfTriangleIndices, sizeof(unsigned int) );

	vector<Mesh::Vertex> vertices( mDescriptor.numberOfVertices );
	vector<Mesh::Normal> normals( mDescriptor.numberOfNormals );
	vector<unsigned int> triangleIndexList( mDescriptor.numberOfTriangleIndices );

	// read out mesh data...
	char *meshName = new char[ mDescriptor.meshNameLength ];
	m_File.read( (char *) meshName, sizeof(char) * mDescriptor.meshNameLength );
	m.setName( meshName );
	delete [] meshName;

	m_File.read( (char *) &vertices[ 0 ], sizeof(float) * 3 * mDescriptor.numberOfVertices );
	m.setVertices( vertices );
	m_File.read( (char *) &normals[ 0 ], sizeof(float) * 3 * mDescriptor.numberOfNormals );
	m.setNormals( normals );

	for( unsigned int i = 0; i < mDescriptor.numberOfUVSets; i++ )
	{
		UVDescriptor uvDescriptor;
		m_File.read( (char *) &uvDescriptor.numberOfUVs, sizeof(unsigned int) );
		vector<Mesh::UV> uvs( uvDescriptor.numberOfUVs );
		m_File.read( (char *) &uvs[ 0 ], sizeof(float) * 2 * uvDescriptor.numberOfUVs );
		m.addUVs( uvs );
	}

	m_File.read( (char *) &triangleIndexList[ 0 ], sizeof(unsigned int) * mDescriptor.numberOfTriangleIndices );
	m.setTriangleIndexList( triangleIndexList );

	//Math::Vector3 m_Translation, m_Rotation, m_Scale;

	float ambient[ 4 ];
	float diffuse[ 4 ];
	float specular[ 4 ];
	float shininess = 0.5f;

	m_File.read( (char *) ambient, sizeof(float) * 4 );
	m.setMaterial( Mesh::MAT_AMBIENT, ambient );
	m_File.read( (char *) diffuse, sizeof(float) * 4 );
	m.setMaterial( Mesh::MAT_DIFFUSE, diffuse );
	m_File.read( (char *) specular, sizeof(float) * 4 );
	m.setMaterial( Mesh::MAT_SPECULAR, specular );
	m_File.read( (char *) &shininess, sizeof(float) );
	m.setShininess( shininess );

	return true;
}

bool FileIO::writeMeshData( const Mesh &m )
{
	MeshDescriptor mDescriptor;

	const Mesh::Vertices &vertices                 = m.vertices( );
	const Mesh::Normals &normals                   = m.normals( );
	const Mesh::IndexCollection &triangleIndexList = m.triangleIndexList( );

	mDescriptor.numberOfVertices        = vertices.size( );
	mDescriptor.numberOfNormals         = normals.size( );
	mDescriptor.numberOfUVSets          = m.numberOfUVTextureSets( );
	mDescriptor.numberOfTriangleIndices = triangleIndexList.size( );
	mDescriptor.meshNameLength          = m.name( ).length( ) + 1;

	// write out the model descriptor...
	m_File.write( (const char *) &mDescriptor.meshNameLength, sizeof(unsigned int) );
	m_File.write( (const char *) &mDescriptor.numberOfVertices, sizeof(unsigned int) );
	m_File.write( (const char *) &mDescriptor.numberOfNormals, sizeof(unsigned int) );
	m_File.write( (const char *) &mDescriptor.numberOfUVSets, sizeof(unsigned int) );
	m_File.write( (const char *) &mDescriptor.numberOfTriangleIndices, sizeof(unsigned int) );


	// write out mesh data...
	m_File.write( (const char *) m.name( ).c_str( ), sizeof(char) * mDescriptor.meshNameLength );
	m_File.write( (const char *) &vertices[ 0 ], sizeof(float) * 3 * mDescriptor.numberOfVertices );
	m_File.write( (const char *) &normals[ 0 ], sizeof(float) * 3 * mDescriptor.numberOfNormals );

	for( unsigned int i = 0; i < mDescriptor.numberOfUVSets; i++ )
	{
		UVDescriptor uvDescriptor;
		const Mesh::UVs &uvs     = m.getUVs( i );
		uvDescriptor.numberOfUVs = uvs.size( );

		m_File.write( (const char *) &uvDescriptor.numberOfUVs, sizeof(unsigned int) );
		m_File.write( (const char *) &uvs[ 0 ], sizeof(float) * 2 * uvDescriptor.numberOfUVs );
	}

	m_File.write( (const char *) &triangleIndexList[ 0 ], sizeof(unsigned int) * mDescriptor.numberOfTriangleIndices );
	

	//MATH::CVector3<float> m_Translation, m_Rotation, m_Scale;
	
	m_File.write( (const char *) m.getMaterial( Mesh::MAT_AMBIENT ), sizeof(float) * 4 );
	m_File.write( (const char *) m.getMaterial( Mesh::MAT_DIFFUSE ), sizeof(float) * 4 );
	m_File.write( (const char *) m.getMaterial( Mesh::MAT_SPECULAR ), sizeof(float) * 4 );
	float shininess = m.getShininess( );
	m_File.write( (const char *) &shininess, sizeof(float) );
	return true;
}

bool FileIO::import( const std::string &filename, Model &model, bool bLoadMayaLibrary )
{
	if( bLoadMayaLibrary ) MLibrary::initialize( NULL );
	MGlobal::displayInfo( "Joe3D Exporter started..." );

	if( MS::kSuccess != MFileIO::open( filename.c_str( ), NULL, true ) )
	{
		std::cerr << "[ERROR] Unable to open " << filename.c_str( ) << std::endl; 
		/*exit( EXIT_FAILURE );*/
		return false;
	}

	// use an iterator to iterate through all nodes in the scene
	MItDag it( MItDag::kDepthFirst, MFn::kMesh );

	while( !it.isDone( ) )
	{
		MFnMesh fn( it.item( ) );
		Mesh theMesh;


		if( fn.isIntermediateObject( ) )
		{ it.next(); continue; }

		cout << "Building MESH " << fn.name( ).asChar( ) << "..." << endl;
		theMesh.setName( fn.name( ).asChar( ) );



		buildMeshGeometry( theMesh, it.item( ) );
		buildMeshTextureData( theMesh, it.item( ) );
		MDagPath path;
		MStatus stat = it.getPath( path );
		assert( stat == MStatus::kSuccess );
		buildMeshMaterialData( model, theMesh, it.item( ), path );



		model.addMesh( theMesh );
		it.next( );
	}

	// close down Maya
	if( bLoadMayaLibrary ) MLibrary::cleanup( 0 );
	return true;
}

void FileIO::buildMeshGeometry( Mesh &mesh, MObject &obj )
{
	Mesh::Vertices meshVerts;
	Mesh::Normals meshNormals;
	vector<unsigned int> meshTriangleIndexList;
	MPointArray verts;
	MFloatVectorArray normals;

	MFnMesh fn( obj );

	
	fn.getPoints( verts, MSpace::kTransform );
	fn.getNormals( normals, MSpace::kTransform );

	for( unsigned int i = 0; i < verts.length( ); i++ )
	{
		Mesh::Vertex v;
		v.x = static_cast<float>( verts[ i ].x );
		v.y = static_cast<float>( verts[ i ].y );
		v.z = static_cast<float>( verts[ i ].z );

		meshVerts.push_back( v );
	}

	unsigned int L = normals.length( );

	for( unsigned int i = 0; i < normals.length( ); i++ )
	{
		Mesh::Normal n;
		normals[ i ].normalize( );
		n.nx = normals[ i ].x;
		n.ny = normals[ i ].y;
		n.nz = normals[ i ].z;

		double magnitude = sqrt( n.nx * n.nx + 
								 n.ny * n.ny + 
								 n.nz * n.nz );
		if( abs( 1 - magnitude ) > 0.000005 )
			cout << "normals maginuted != 1.0 but " << magnitude << endl;

		meshNormals.push_back( n );
	}

	mesh.setVertices( meshVerts );
	mesh.setNormals( meshNormals );

	MItMeshPolygon polyIt( obj );

	

	while( !polyIt.isDone( ) )
	{
		MPointArray trianglePoints;
		MIntArray triangleIndexList;
		polyIt.getTriangles( trianglePoints, triangleIndexList, MSpace::kTransform );

		for( unsigned int q = 0; q < triangleIndexList.length(); q++ )
			meshTriangleIndexList.push_back( triangleIndexList[ q ] );

		polyIt.next( );
	}

	mesh.setTriangleIndexList( meshTriangleIndexList );
}

void FileIO::buildMeshTextureData( Mesh &mesh, MObject &obj )
{
	MFnMesh fn( obj );

	MStringArray uvSetNames;
	unsigned int numberOfUVSets = fn.numUVSets( );
	unsigned int numberOfFaces = fn.numPolygons( );
	MFloatArray *uvs[ 2 ]; // 0 = u, 1 = v
	uvs[ 0 ] = new MFloatArray[ numberOfUVSets ];
	uvs[ 1 ] = new MFloatArray[ numberOfUVSets ];

	fn.getUVSetNames( uvSetNames );

	for( unsigned int i = 0; i < numberOfUVSets; i++ )
		fn.getUVs( uvs[ 0 ][ i ], uvs[ 1 ][ i ], &uvSetNames[ i ] );

	for( unsigned int idx = 0; idx < numberOfUVSets; idx++ )
	{
		vector<Mesh::UV> theUVs;

		for( unsigned int k = 0; k < uvs[ 0 ][ idx ].length( ); k++ )
		{
			Mesh::UV uv;

			uv.u = uvs[ 0 ][ idx ][ k ];
			uv.v = uvs[ 1 ][ idx ][ k ];
			theUVs.push_back( uv );
		}

		mesh.addUVs( theUVs );
	}
}

void FileIO::buildMeshMaterialData( Model &model, Mesh &mesh, MObject &obj, MDagPath &path )
{
	MStatus stat = MStatus::kSuccess;
	MFnMesh fn( obj );
	MObjectArray shaders;
	MIntArray indices;
	unsigned int numberOfMaterialsFound = 0;


	m_bTextured = false;
	m_bMultiTextured = false;

	fn.getConnectedShaders( path.instanceNumber( ), shaders, indices );

	for( unsigned int i = 0; i < shaders.length( ); i++ )
	{

		MFnDependencyNode fnDepNode( shaders[ i ] );
		MPlug plug = fnDepNode.findPlug( "surfaceShader", &stat );
		CHECK_MSTATUS( stat );
		if( plug.isNull( ) ) continue;

		//cout <<  "fnDepNode   " << fnDepNode.name().asChar() << endl;

		MPlugArray shaderPlugs;
		plug.connectedTo( shaderPlugs, true, false, &stat );
		CHECK_MSTATUS( stat );

		

		for( unsigned int j = 0; j < shaderPlugs.length( ); j++ )
		{
			MObject shaderObject = shaderPlugs[ j ].node( );


			MFnDependencyNode fnColorSrcPlugs( shaderObject );
			MPlugArray colorSrcPlugs;
			// Check if material is textured
			fnColorSrcPlugs.findPlug("color").connectedTo( colorSrcPlugs, true, false );

			for( unsigned int i = 0; i < colorSrcPlugs.length( ); i++ )
			{
				if( colorSrcPlugs[ i ].node( ).hasFn( MFn::kFileTexture ) )
				{
					m_bTextured = true;
					continue;
				}
				else if( colorSrcPlugs[ i ].node( ).hasFn( MFn::kLayeredTexture ) )
				{
					m_bTextured = true;
					m_bMultiTextured = true;
					continue;
				}
			}

			if( !buildSurfaceShader( mesh, shaderObject, numberOfMaterialsFound ) )
				continue;


			if( m_bMultiTextured )
			{
				MFnDependencyNode fnTextureDepNode( shaderObject );
				MPlug color = fnTextureDepNode.findPlug( "color", &stat );
				CHECK_MSTATUS( stat );
				MPlugArray layeredTexturePlugs;
				color.connectedTo( layeredTexturePlugs, true, false, &stat );
				CHECK_MSTATUS( stat );


				for( unsigned int k = 0; k < layeredTexturePlugs.length( ); k++ )
				{
					MObject textureObject = layeredTexturePlugs[ k ].node( );
					MFnDependencyNode fnLayeredTexNode( textureObject );
				
					// Get inputs to layered texture
					MPlug inputsPlug = fnLayeredTexNode.findPlug( "inputs" );

					unsigned int numberOfTextureLayers = inputsPlug.numElements( );

					for( int l = inputsPlug.numElements( ) - 1; l >= 0; l-- )
					{
						//cout << "\tDEBUG   " << inputsPlug[ l ].info( ).asChar() << endl;
						MPlug color = inputsPlug[ l ].child( 0 );
						CHECK_MSTATUS( stat );
						MPlugArray texturePlugs;
						color.connectedTo( texturePlugs, true, false, &stat );
						CHECK_MSTATUS( stat );


						for( unsigned int k = 0; k < texturePlugs.length( ); k++ )
						{
							MObject textureObject = texturePlugs[ k ].node( );
							MFnDependencyNode fnTextureFileDepNode( textureObject );

							MPlug ftn = fnTextureFileDepNode.findPlug( "ftn", &stat );
							if( ftn.isNull( ) ) continue;
							CHECK_MSTATUS( stat );
							MString textureFilename;
							ftn.getValue( textureFilename );
							//cout << "MultiTexture Filename = " << textureFilename.asChar( )  << endl;
							model.addTexture( string(textureFilename.asChar()) );
						}
					}

				}
			}
			else if( m_bTextured ) // single textured...
			{ 
				MFnDependencyNode fnTextureDepNode( shaderObject );
				MPlug color = fnTextureDepNode.findPlug( "color", &stat );
				CHECK_MSTATUS( stat );
				MPlugArray texturePlugs;
				color.connectedTo( texturePlugs, true, false, &stat );
				CHECK_MSTATUS( stat );
				

				for( unsigned int k = 0; k < texturePlugs.length( ); k++ )
				{
					MObject textureObject = texturePlugs[ k ].node( );
					MFnDependencyNode fnTextureFileDepNode( textureObject );

					MPlug ftn = fnTextureFileDepNode.findPlug( "ftn", &stat );
					if( ftn.isNull( ) ) continue;
					CHECK_MSTATUS( stat );
					MString textureFilename;
					ftn.getValue( textureFilename );
					//cout << "Texture Filename = " << textureFilename.asChar( )  << endl;
					model.addTexture( string(textureFilename.asChar()) );
				}
			}
		}
	}
}

bool FileIO::buildSurfaceShader( Mesh &mesh, MObject &obj, unsigned int &numberOfMaterialsFound )
{
	float ambientMaterial[ 4 ] = {0};
	float diffuseMaterial[ 4 ] = {0};
	float specularMaterial[ 4 ] = {0};
	float shininess = 0.5f;

	switch( obj.apiType( ) )
	{
		case MFn::kLambert:
			{
				MFnLambertShader lambertFn( obj );
				MColor ambient = lambertFn.ambientColor( );
				MColor diffuse = lambertFn.diffuseCoeff( ) * lambertFn.color( );
				MColor transparency = lambertFn.transparency( );
				// no specular material here for lambert

				for( unsigned int k = 0; k < 4; k++ )
					ambientMaterial[ k ] = ambient[ k ];
				for( unsigned int k = 0; k < 4; k++ )
					diffuseMaterial[ k ] = diffuse[ k ];
				for( unsigned int k = 0; k < 4; k++ )
					specularMaterial[ k ] = 0.0f;

				break;
			}

		case MFn::kPhong:
			{
				MFnPhongShader phongFn( obj );
				MColor ambient = phongFn.ambientColor( );
				MColor diffuse = phongFn.diffuseCoeff( ) * phongFn.color( );
				MColor specular = phongFn.specularColor( );
				MColor transparency = phongFn.transparency( );

				for( unsigned int k = 0; k < 4; k++ )
					ambientMaterial[ k ] = ambient[ k ];
				for( unsigned int k = 0; k < 4; k++ )
					diffuseMaterial[ k ] = diffuse[ k ];
				for( unsigned int k = 0; k < 4; k++ )
					specularMaterial[ k ] = 0.0f;

				shininess = phongFn.cosPower( );

				break;
			}

		case MFn::kBlinn:
			{
				MFnBlinnShader blinnFn( obj );
				MColor ambient = blinnFn.ambientColor( );
				MColor diffuse = blinnFn.diffuseCoeff( ) * blinnFn.color( );
				MColor specular = blinnFn.specularColor( );
				MColor transparency = blinnFn.transparency( );

				for( unsigned int k = 0; k < 4; k++ )
					ambientMaterial[ k ] = ambient[ k ];
				for( unsigned int k = 0; k < 4; k++ )
					diffuseMaterial[ k ] = diffuse[ k ];
				for( unsigned int k = 0; k < 4; k++ )
					specularMaterial[ k ] = 0.0f;

				shininess = blinnFn.specularRollOff( );

				break;
			}
		default:
			return false;
	} // end of switch

	mesh.setMaterial( Mesh::MAT_AMBIENT, ambientMaterial );

	if( m_bTextured )
	{
		float diffuseColor[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		mesh.setMaterial( Mesh::MAT_DIFFUSE, diffuseColor );
	}
	else
		mesh.setMaterial( Mesh::MAT_DIFFUSE, diffuseMaterial );

	mesh.setMaterial( Mesh::MAT_SPECULAR, specularMaterial );
	mesh.setShininess( shininess );
	numberOfMaterialsFound++;
	assert( numberOfMaterialsFound <= 1 );

	return true;
}

} // end of namespace Joe3D
