#include "Model.h"

namespace Joe3D {

Model::Model( )
{
}

Model::~Model( )
{
}

void Model::addMesh( const Mesh &mesh )
{
	m_Meshes.push_back( mesh );
}

void Model::addTexture( const string &textureFilename )
{
	m_Textures.push_back( textureFilename );
}

void Model::setName( const string &name )
{ m_Name = name; }

string &Model::name( )
{ return m_Name; }

string Model::name( ) const
{ return m_Name; }

void Model::render( )
{
	for( unsigned int i = 0; i < m_Meshes.size( ); i++ )
		m_Meshes[ i ].render( );
}

void Model::clear( )
{
	m_Meshes.clear( );
	m_Name = "";
}

Mesh &Model::operator[]( int idx )
{ 
	return m_Meshes[ idx ];
}

const Mesh &Model::operator[]( int idx ) const
{
	return m_Meshes[ idx ]; 
}

} // end of namespace Joe3D
