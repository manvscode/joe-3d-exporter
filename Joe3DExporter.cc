/*
 *	Joe3DExporter.cc
 * 
 *	The Joe3D model exporter.
 *
 *	Coded by Joseph A. Marrero
 *	5/10/2007
 */


#include <cassert>
#include <ostream>
#include <iostream>
#include <fstream>
#include <GL/glew.h>
#include <GL/glut.h>
#ifndef MLIBRARY_DONTUSE_MFC_MANIFEST
#define MLIBRARY_DONTUSE_MFC_MANIFEST
#endif
#include <maya/MTypes.h>
#include <maya/MLibrary.h>

#include "Joe3DExporter.h"
using namespace std;



int main( int argc, char *argv[] )
{
	getCommandLine( &argc, argv );
	glutInit( &argc, argv );
	glutInitDisplayMode( GLUT_RGBA | GLUT_DOUBLE );
	glutInitWindowSize( 640, 480 );
	
	glutCreateWindow( "Joe3D Exporter" );
	//glutFullScreen( );

	GLenum err = glewInit( );
	if( GLEW_OK != err )
	{
		/* Problem: glewInit failed, something is seriously wrong. */
		cerr << "Error: " << glewGetErrorString( err ) << endl;
		return -1;
	}

	glutDisplayFunc( render );
	glutReshapeFunc( resize );
	glutKeyboardFunc( keyboard_keypress );
	glutIdleFunc( idle );

	// initialize the Maya library - This basically starts up Maya
	MLibrary::initialize( argv[ 0 ], false );
	
	initialize( );
	glutMainLoop( );
	deinitialize( );

	return 0;
}

void getCommandLine( int *argc, char *argv[] )
{
#ifndef _DEBUG
	if( *argc < 3 )
	{
		cerr << "[ERROR] Usage\n\t" << argv[ 0 ] << " [infile.mb|infile.ma] [outfile]\n" << endl;
		exit( EXIT_FAILURE );
	}
	else 
	{
		mayaFilename.assign( argv[ 1 ] );
		outputFilename.assign( argv[ 2 ] );
	}
#endif
}

void initialize( )
{
	mayaFilename.assign( "../sub.mb" );
	outputFilename.assign( "sub.joe3d" );

	//glEnable( GL_ALPHA_TEST );
	
	glEnable( GL_POINT_SMOOTH );
	glEnable( GL_LINE_SMOOTH );
	glHint( GL_POINT_SMOOTH_HINT, GL_FASTEST );
	glHint( GL_LINE_SMOOTH_HINT, GL_FASTEST );
	glHint( GL_POLYGON_SMOOTH_HINT, GL_FASTEST );
	glHint( GL_PERSPECTIVE_CORRECTION_HINT, GL_FASTEST );
	
	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	
	glEnable( GL_NORMALIZE );

	glEnable( GL_CULL_FACE );
	glFrontFace( GL_CCW );

	glShadeModel( GL_SMOOTH );
	glClearDepth( 1.0f );		
	glDepthFunc( GL_LEQUAL );
	glEnable( GL_DEPTH_TEST );

	glEnable( GL_COLOR_MATERIAL );
	glEnable( GL_LIGHTING );
	glEnable( GL_LIGHT0 );
	//glEnable( GL_LIGHT1 );

	glPointSize( 3.0f );
	glEnable( GL_TEXTURE_2D );

	glEnableClientState( GL_VERTEX_ARRAY );
	glEnableClientState( GL_NORMAL_ARRAY );
	glEnableClientState( GL_TEXTURE_COORD_ARRAY );
	//glEnableClientState( GL_INDEX_ARRAY );

	glPixelStorei( GL_PACK_ALIGNMENT, 4 );
	glPixelStorei( GL_UNPACK_ALIGNMENT, 4 );

	// open the Maya file
	Joe3D::FileIO::import( mayaFilename, theModel );

	theModel.setName( string("Barracuda Submarine") );

	for( int i = 0; i < theModel.numberOfMeshes( ); i++ )
	{
		Joe3D::Mesh &m = theModel[ i ];
		Joe3D::Mesh::Normals &normals = m.normals( );

		for( int j = 0; j < normals.size( ); j++ )
		{
			double magnitude = sqrt( normals[ j ].nx * normals[ j ].nx + 
									 normals[ j ].ny * normals[ j ].ny + 
									 normals[ j ].nz * normals[ j ].nz );
			if( (1.0f - magnitude) > 0.000005 )
				cout << "normals Length <= 1.0 but " << magnitude << endl;
		}
	}

	Joe3D::FileIO fileOut( outputFilename, Joe3D::FileIO::JOE3D_OUT );
	fileOut.write( theModel );
}

void deinitialize( )
{

	MLibrary::cleanup( 0 );

}


void render( )
{
	static float angle            = 0.0f;
	static float lightDirection[] = { 0.0f, -1.0f, -0.5f, 0.0f };
	static float lightPosition[]  = { 0.0f, 10.0f, 5.0f, 1.0f };
	static float lightDiffuse[]   = { 0.7f, 0.7f, 0.7f, 0.0f };
	static float lightAmbient[]   = { 0.05f, 0.05f, 0.05f, 0.0f };

	glClearColor( 0.3f, 0.3f, 0.3f, 0.0f );
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	glLoadIdentity( );


	glTranslatef( 0.0f, 0.0f, -15.0f );
	glRotatef( angle++, 0.0f, 1.0f, 0.0f );
	glRotatef( 20.0f, 1.0f, 0.0f, 1.0f );
	
	
	if( angle >= 360.0f ) angle = 0.0f;

	// TO DO: Drawing code goes here...
	
	glPolygonMode( GL_FRONT, GL_LINE );

	glColor3f( 0.8f, 0.8f, 0.8f );
	theModel.render( );

	glLightfv( GL_LIGHT0, GL_DIFFUSE, lightDiffuse );
	glLightfv( GL_LIGHT0, GL_AMBIENT, lightAmbient );

	/* Write text */
	int width = glutGet((GLenum)GLUT_WINDOW_WIDTH);
	int height = glutGet((GLenum)GLUT_WINDOW_HEIGHT);

	writeText( GLUT_BITMAP_HELVETICA_18, std::string("Joe 3D Exporter: ") + theModel.name( ), 2, 22 );
	writeText( GLUT_BITMAP_9_BY_15, std::string("Press Q to quit."), 2, 5 );
	
	glutSwapBuffers( );

	//assert( glGetError() == GL_NO_ERROR );
}

void resize( int width, int height )
{
	glViewport( 0, 0, width, height );
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity( );


	if( height == 0 )
		height = 1;

	gluPerspective( 45.0f, (double) width / (double) height, 1.0, 100.0 );
	glMatrixMode( GL_MODELVIEW );
}

void keyboard_keypress( unsigned char key, int x, int y )
{
	switch( key )
	{
		case 'Q':
		case 'q':
		case GLUT_KEY_F1:
		case GLUT_KEY_END:
			deinitialize( );
			exit( 0 );
			break;
		default:
			break;
	}

}

void idle( )
{ glutPostRedisplay( ); }

void writeText( void *font, std::string &text, int x, int y )
{
	int width = glutGet( (GLenum) GLUT_WINDOW_WIDTH );
	int height = glutGet( (GLenum) GLUT_WINDOW_HEIGHT );

	glDisable( GL_DEPTH_TEST );

	glPushAttrib( GL_LIGHTING_BIT | GL_TEXTURE_BIT );
		
		glDisable( GL_TEXTURE_2D );
		glDisable( GL_LIGHTING );

		glMatrixMode( GL_PROJECTION );
		glPushMatrix( );
			glLoadIdentity( );	
			glOrtho( 0, width, 0, height, 1.0, 10.0 );
				
			glMatrixMode( GL_MODELVIEW );
			glPushMatrix( );
				glLoadIdentity( );
				glColor3f( 1.0f, 1.0f, 1.0f );
				glTranslatef( 0.0f, 0.0f, -1.0f );
				glRasterPos2i( x, y );

				for( unsigned int i = 0; i < text.size( ); i++ )
					glutBitmapCharacter( font, text[ i ] );
				
			glPopMatrix( );
			glMatrixMode( GL_PROJECTION );
		glPopMatrix( );
		glMatrixMode( GL_MODELVIEW );

		//glEnable( GL_LIGHTING );
		
		//glEnable( GL_TEXTURE_2D );
	glPopAttrib( );
	glEnable( GL_DEPTH_TEST );
}

