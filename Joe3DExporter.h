#ifndef _JOE3DEXPORTER_H_
#define _JOE3DEXPORTER_H_
/*
 *	Joe3DExporter.h
 *
 *	The Joe3D model exporter.
 *
 *	Coded by Joseph A. Marrero
 *	5/10/2007
 */
#include <string>
#include "Joe3D/Model.h"
#include "Joe3D/Mesh.h"
using namespace std;

void getCommandLine( int *argc, char *argv[] );
void initialize( );
void deinitialize( );

void render( );
void resize( int width, int height );
void keyboard_keypress( unsigned char key, int x, int y );
void idle( );
void writeText( void *font, std::string &text, int x, int y );


string mayaFilename;
string outputFilename;
Joe3D::Model theModel;

#endif
